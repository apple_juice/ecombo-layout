"use strict";

window.addEventListener('DOMContentLoaded', function () {
  function isMobile() {
    return (/Android|iPhone|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }

  const BODY = $('body');

  (function() {
    BODY.addClass('loaded');

    if(isMobile()){
      BODY.addClass('body--mobile');
    }else{
      BODY.addClass('body--desktop');
    }
  })();

  // INTRO VIDEO

  const intro = (function(){
    let canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

    function init() {
      canvas = document.getElementById("canvas");
      anim_container = document.getElementById("animation_container");
      dom_overlay_container = document.getElementById("dom_overlay_container");
      var comp=AdobeAn.getComposition("AC7D1377BDBCD644A55932EB4718E9F2");
      var lib=comp.getLibrary();
      createjs.MotionGuidePlugin.install();
      var loader = new createjs.LoadQueue(false);
      loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
      loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
      var lib=comp.getLibrary();
      loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
      var images=comp.getImages();
      if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
      //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
      var lib=comp.getLibrary();
      var ss=comp.getSpriteSheet();
      var queue = evt.target;
      var ssMetadata = lib.ssMetadata;
      for(var i=0; i<ssMetadata.length; i++) {
        ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
      }
      exportRoot = new lib.konveer();
      stage = new lib.Stage(canvas);
      //Registers the "tick" event listener.
      fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
      }
      //Code to support hidpi screens and responsive scaling.
      function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
          var w = lib.properties.width, h = lib.properties.height;
          var iw = window.innerWidth, ih=window.innerHeight;
          var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
          if(isResp) {
            if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
              sRatio = lastS;
            }
            else if(!isScale) {
              if(iw<w || ih<h)
                sRatio = Math.min(xRatio, yRatio);
            }
            else if(scaleType==1) {
              sRatio = Math.min(xRatio, yRatio);
            }
            else if(scaleType==2) {
              sRatio = Math.max(xRatio, yRatio);
            }
          }
          canvas.width = w*pRatio*sRatio;
          canvas.height = h*pRatio*sRatio;
          canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
          canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
          stage.scaleX = pRatio*sRatio;
          stage.scaleY = pRatio*sRatio;
          lastW = iw; lastH = ih; lastS = sRatio;
          stage.tickOnUpdate = false;
          stage.update();
          stage.tickOnUpdate = true;
        }
      }
      makeResponsive(true,'both',false,1);
      AdobeAn.compositionLoaded(lib.properties.id);
      fnStartAnimation();
    }

    return {
      init: init
    }
  }());

  // SLICK SLIDER

  const slider = (function () {
    function init() {
      $('.slick_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
      });
    }
    return {
      init: init
    }
  })();

  const mobileSlider = (function () {
    function init() {
      $('._remove_on_mobile').remove();

      $('._features_slider, ._pricing_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
        arrows: false
      });

      $('._features_slider_alt').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        adaptiveHeight: true,
        dots: true,
        arrows: false
      });
    }
    return {
      init: init
    }
  })();

  const contentAnimation = (function () {
    function init() {
      $('.inview').on('inview', function(event, visible, topOrBottomOrBoth) {
        if (visible == true) {
          // element is now visible in the viewport
          if (topOrBottomOrBoth == 'top') {
            // top part of element is visible
          } else if (topOrBottomOrBoth == 'bottom') {
            // bottom part of element is visible
          } else {
            $(this).addClass('show')
          }
        } else {
          // element has gone out of viewport
        }
      });
    }
    return {
      init: init
    }
  })();


  const mapBubbles = (function () {
    function init(container, count) {
      let activeLayer = 0

      function fade() {
        container.removeClass('map_layer--' + activeLayer)
        activeLayer = (activeLayer + 1) % count
        container.addClass('map_layer--' + activeLayer)
      }

      setInterval(fade, 3000)
    }
    return {
      init: init
    }
  })();

  $("._hover_animation").hover(function(){
    $(".trump").addClass("trump--show");
  }, function () {
    $(".trump").removeClass("trump--show");
  });


  const scroll = new SmoothScroll('a[data-scroll]');

  intro.init();
  slider.init();

  const mapAnimationContainer = $('._map');
  mapAnimationContainer ? mapBubbles.init(mapAnimationContainer, 3) : null;

  const inview = $('.inview');
  inview ? contentAnimation.init() : null;

  return isMobile() ? mobileSlider.init() : null;
});
