(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.Image = function() {
	this.initialize(img.Image);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,568,100);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#94DEA8").s().p("AgVAoIgBAAQgKgGgFgJQgEgGgCgHIAAABQgEgRAJgRIAAAAQAJgQATgFQALgDAMAEIgVAmIAQAJIAXglQAHAGADAJIABABIAAAAIABAFIAAABQAFAQgJAQQgKAQgRAFQgHACgFAAQgLAAgKgGgAAmgtIABABIgBAAg");
	this.shape.setTransform(54.7,58);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BDE2F4").s().p("ACjCBIlGAAQgjgBgZgYIAAAAQgYgZAAgkIAAhVQAAgjAYgZIAAAAQAZgaAjAAIFGAAQAkAAAYAaIAAAAQAaAZAAAjIAABVQAAAkgaAZQgYAZgjAAIgBAAgAA6AGQARARAZAAQAZAAARgRQASgRAAgZIgUAAQAAARgLAMQgMALgRAAQgQAAgNgLQgLgMAAgRIgUAAQAAAZASARgAiOAGQASARAZAAQAYAAASgRQASgRAAgZIgUAAQAAARgMAMQgMALgQAAQgRAAgMgLQgLgMAAgRIgUAAQAAAZARARg");
	this.shape_1.setTransform(70.9,27.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF8B9").s().p("AKjGoIAFgPIAQgDIAKAMIgFAPIgQACgArBGoIAFgPIAPgDIALALIgFAQIgQACgAKjmgIAFgPIAQgEIAKANIgFAOIgQAEgArBmgIAFgPIAPgEIALANIgFAOIgQAEg");
	this.shape_2.setTransform(87.2,58.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F79F9A").s().p("AMMI2IAAhTQAKgNAFgOIAAAAQAEgOAAgRIAAtRQAAgRgEgOIAAAAQgFgOgKgNIAAhTIAMAAQAcAAATATQATAUAAAaIAAPoQAAAbgTATIAAABIgBAAQgTATgbAAgAsYI2QgbAAgTgTIAAAAQgTgUAAgbIAAvoQAAgaATgUIAAAAQATgTAbAAIANAAIAABSQgJAMgFAOIAAAAIAAAAQgFAPAAASIAANRQAAASAFAPIAAAAIAAAAQAFAOAJAMIAABSg");
	this.shape_3.setTransform(87.3,58.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F8D4D8").s().p("ALAHrIgLAAIAAgaIAegGIAQgwIgigmIgMADIAAruIAegHIAQgvIgignIgMADIAAgaIALAAQAbAAATATIAAAAQAMAMAEANIAAABQADAJAAAMIAANSQAAALgDAKIAAAAQgEAOgMALQgTATgbAAIAAAAgArtHYQgLgKgFgOIAAAAQgDgKAAgMIAAtSQAAgMADgLIAAABQAFgOALgLIAAAAQATgTAbAAIALAAIAAAbIggAHIgQAwIAhAmIAPgDIAALuIggAGIgQAwIAhAmIAPgDIAAAZIgLAAIAAAAQgbAAgTgTg");
	this.shape_4.setTransform(87.3,58.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF6770").s().p("AAAAtIAAAAQgSAAgOgOIgDgFIgBgBQgIgLAAgOIAAAAQAAgSAOgNIAAAAIAAAAQANgNASAAIAAAAQANAAAKAHIgeAfIANANIAfgdIACACQAFAJAAALIAAABQAAASgOANIAAAAQgMANgSAAIgBAAg");
	this.shape_5.setTransform(54.6,95.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#40516C").s().p("AsYJUQgoAAgbgdIAAAAQgcgbAAgoIAAvoQAAgnAcgcIAAAAQAcgcAnAAIYwAAQAoAAAcAcQAcAcAAAnIAAPoQAAAogcAbIAAABQgcAcgoAAgAtGoiIAAAAQgTAUAAAaIAAPoQAAAbATAUIAAAAQATATAbAAIANAAIYXAAIAMAAQAbAAATgTIABAAIAAgBQATgTAAgbIAAvoQAAgagTgUQgTgTgcAAIgMAAI4XAAIgNAAIAAAAQgbAAgTATgAq/IJQgnAAgcgcIgJgJQgJgMgFgOIAAAAIAAAAQgFgPAAgSIAAtRQAAgSAFgPIAAAAIAAAAQAFgOAJgMIAJgJIAAAAQAbgdAoAAIV/AAQAoAAAbAdIAAAAIAJAKQAKANAFAOIAAAAQAEAOAAARIAANRQAAARgEAOIAAAAQgFAOgKANIgJAKQgcAcgnAAgAq0HrIVpAAIALAAQAbAAATgTQAMgLAEgOIAAAAQADgKAAgMIAAtRQAAgMgDgKIAAAAQgEgOgMgLIAAAAQgTgTgbgBIgLAAI1pAAIgLAAQgbABgTATIAAAAQgLALgFANIAAAAQgDALAAAMIAANRQAAAMADALIAAAAQAFANALALQATATAbAAgAKAGvIAQgwIAlgHIAMgDIAiAmIgQAwIgeAGIgUAEgAKnGZIgFAPIAKAMIAQgDIAFgPIgLgMgArkGvIAQgwIAggGIARgEIAiAmIgRAwIgiAHIgPADgAq9GZIgFAPIAKAMIAPgDIAFgPIgKgMgAlHHAIAAAAQgPAAgMgFQgOgGgMgMIgIgKIABABQgOgTAAgZIAAAAQAAgfAXgVIAAAAQAWgWAfAAIAAAAQAZABATAOIgJALIAFgEIAOANIgGAGIANgIIADAFQAKARAAAVIAAABIgBAEQgBAcgVAUIAAgBQgVAWgeAAIgCAAgAllFVIgBAAIAAAAQgNANAAASIAAABQAAAPAIALIABAAIADAFQAOAOASAAIABAAQASAAANgNIAAAAQAOgNAAgSIAAAAQAAgNgGgJIgBgCIgfAeIgOgOIAfgfQgKgHgNABIAAAAIgCgBQgSAAgMANgAi8GHIAAghIC0AAIAAAhgAlXEBQgNgEgKgIQgNgKgIgQIAAABIgEgKIAAAAQgHgSADgSIAPADIACABIgCgBIgHgDIAGgTIAGADIgMgGQAKgWAWgLIAAAAQAbgPAeAKQAeAJAOAcIAAAAIAAAAQAEAIADAJIAAAAQAFATgHAVIAAAAIgBACQgJAcgbAOIAAAAQgQAIgSAAQgLAAgMgDgAlrDHIABACIABAEIAAAAIAAABQAJARASAFQASAGAQgJIABAAQAQgIAFgSIAAAAQAFgMgDgLIgFgMIABAAQgJgQgSgGQgSgFgQAIIgBAAQgLAGgGALIAqAOIgGATIgqgOQgBAJADAJgAJqD5QgFAAgEgEQgEgDAAgFIAAnrQAAgFAEgEQAEgEAFAAIAKAAQAFAAADAEQAEAEAAAFIAAHrQAAAFgEADQgDAEgFAAgAIuD5QgFAAgDgEQgEgDAAgFIAAnrQAAgFAEgEQADgEAFAAIALAAQAFAAADAEQAEAEAAAFIAAHrQAAAFgEADQgDAEgFAAgAHzD5QgFAAgDgEQgEgDAAgFIAAnrQAAgFAEgEQADgEAFAAIAKAAQAFAAAEAEQAEAEAAAFIAAHrQAAAFgEADQgEAEgFAAgAG4D5QgFAAgDgEQgEgDAAgFIAAnrQAAgFAEgEQADgEAFAAIAKAAQAFAAAEAEQADAEAAAFIAAHrQAAAFgDADQgEAEgFAAgAF9D5QgFAAgDgEQgEgDAAgFIAAnrQAAgFAEgEQADgEAFAAIAKAAQAFAAAEAEQADAEAAAFIAAHrQAAAFgDADQgEAEgFAAgAFCD5QgFAAgEgEQgDgDAAgFIAAnrQAAgFADgEQAEgEAFAAIAKAAQAFAAAEAEQADAEAAAFIAAHrQAAAFgDADQgEAEgFAAgAEDD5QgFAAgEgEQgDgDAAgFIAAnrQAAgFADgEQAEgEAFAAIAKAAQAFAAAEAEQADAEAAAFIAAHrQAAAFgDADQgEAEgFAAgADAD5QgFAAgEgEQgDgDAAgFIAAnrQAAgFADgEQAEgEAFAAIAKAAQAFAAADAEQAEAEAAAFIAAHrQAAAFgEADQgDAEgFAAgAi8DNIAAgiIC0AAIAAAigAlqBCIgBgBIABAAQgSgJgJgQQgGgJgDgLIAAAAQgHgdAPgbIAAAAIAAAAQAPgbAfgJQAYgGAWAJIgFAMIADgFIARAKIgEAHIAAABIAAgBIAJgMQAPALAHATIAAAAIADAJIAAAAQAFAQgDAOQgCANgHANQgRAbgdAHQgLADgKAAQgSAAgRgJgAlrAYQAFAJAKAGIABAAQAPAJASgEQASgFAKgRQAJgPgFgRIAAAAIgBgGIAAAAIgBAAQgDgKgIgGIgWAlIgRgJIAWgmQgMgEgMADQgTAFgJARIAAgBQgJARAEARIAAAAQACAGAEAGgAi8ARIAAggIC0AAIAAAggAlHiQQgvABgigiIAAAAIAAAAQgigiAAgwIAAhWQAAgwAighIAAAAQAigiAvAAIFHAAQAvAAAhAiIAAAAQAiAhAAAwIAABWQAAAwgiAiQghAhgvAAgAmDmVIAAAAQgZAZAAAjIAABWQAAAkAZAZIAAAAQAZAYAjABIFHAAQAjAAAYgZQAZgZAAgkIAAhWQAAgjgZgZIAAAAQgZgagiAAIlHAAIAAAAQgjAAgZAagAhqkoQgRgSAAgZIAUAAQAAARALAMQAMAMARAAQAQAAAMgMQAMgMAAgRIAUAAQAAAZgSASQgSARgYAAQgZAAgSgRgAkykoQgRgSAAgZIAUAAQAAARALAMQAMAMARAAQAQAAAMgMQAMgMAAgRIAUAAQAAAZgSASQgSARgYAAQgZAAgSgRgAKAmZIAQgvIAlgJIAMgCIAiAnIgQAvIgeAGIgUAFgAKnmvIgFAQIAKALIAQgDIAFgPIgLgMgArkmZIAQgvIAggHIARgEIAiAnIgRAvIgiAHIgPAEgAq9mvIgFAQIAKALIAPgDIAFgPIgKgMg");
	this.shape_6.setTransform(87.3,58.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#05B8E5").s().p("AgFArQgSgGgJgQIAAgBIAAAAIgCgEIAAgCQgEgJABgIIApAMIAGgSIgpgNQAGgLAMgGIAAAAQAQgJASAGQARAGAJAQIAAAAIAEALQADALgEAMIAAAAQgFARgRAJIAAAAQgKAFgLAAQgGAAgGgCgAgzgHIAAAAIACAAg");
	this.shape_7.setTransform(54.5,76.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DF9E92").s().p("AgkDWIgKgGQANAFAPAAIAAAAQAeABAWgWIAAAAQAVgUABgcQAGAZgMAWQgOAXgZAHQgJACgHAAQgRAAgOgJgAgkAdQgOgIgHgLQAKAIANAEQAeAJAagOIAAAAQAbgOAJgaQAAAPgIAOQgOAXgZAHQgJACgHAAQgRAAgOgJgAgkibQgKgFgHgIQAaAPAdgIQAdgIARgbQAHgNACgNQAEAXgLAVQgOAXgZAHQgJACgHAAQgRAAgOgJg");
	this.shape_8.setTransform(56.4,81.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F5BDAD").s().p("Aq0HrIAAgZIAigHIARgwIgigmIgRAEIAAruIAigIIARgvIgignIgRAEIAAgbIVpAAIAAAaIglAIIgQAwIAhAmIAUgEIAALuIglAHIgQAwIAgAmIAVgEIAAAagAl8GqQAMALAOAGIAJAGQAWAOAagHQAZgHAOgXQAMgWgGgZIABgEIAAgBQAAgVgKgQIgDgGIgNAIIAGgFIgOgOIgFAEIAJgLQgTgOgZAAIAAAAQgfgBgWAWIAAAAQgXAVAAAgIAAAAQAAAZAOATIgBgBIAIAKgAi8GHIC0AAIAAghIi0AAgAluD1QAIAMANAHQAWAOAagHQAZgHAOgXQAIgPAAgPIABgDIAAAAQAHgUgFgUIAAAAQgDgJgEgIIAAAAIAAAAQgOgcgegJQgegJgbAOIAAAAQgWALgKAWIAMAGIgGgCIgGATIAHACIAAAAIgPgCQgDARAHASIAAABIAEAJIAAAAQAIAPANAKgAJhkHQgEAEAAAFIAAHrQAAAFAEAEQAEADAFAAIAKAAQAFAAADgDQAEgEAAgFIAAnrQAAgFgEgEQgDgDgFAAIgKAAQgFAAgEADgAImkHQgEAEAAAFIAAHrQAAAFAEAEQADADAFAAIALAAQAFAAADgDQAEgEAAgFIAAnrQAAgFgEgEQgDgDgFAAIgLAAQgFAAgDADgAHrkHQgEAEAAAFIAAHrQAAAFAEAEQADADAFAAIAKAAQAFAAAEgDQAEgEAAgFIAAnrQAAgFgEgEQgEgDgFAAIgKAAQgFAAgDADgAGwkHQgEAEAAAFIAAHrQAAAFAEAEQADADAFAAIAKAAQAFAAAEgDQADgEAAgFIAAnrQAAgFgDgEQgEgDgFAAIgKAAQgFAAgDADgAF1kHQgEAEAAAFIAAHrQAAAFAEAEQADADAFAAIAKAAQAFAAAEgDQADgEAAgFIAAnrQAAgFgDgEQgEgDgFAAIgKAAQgFAAgDADgAE5kHQgDAEAAAFIAAHrQAAAFADAEQAEADAFAAIAKAAQAFAAAEgDQADgEAAgFIAAnrQAAgFgDgEQgEgDgFAAIgKAAQgFAAgEADgAD6kHQgDAEAAAFIAAHrQAAAFADAEQAEADAFAAIAKAAQAFAAAEgDQADgEAAgFIAAnrQAAgFgDgEQgEgDgFAAIgKAAQgFAAgEADgAC3kHQgDAEAAAFIAAHrQAAAFADAEQAEADAFAAIAKAAQAFAAADgDQAEgEAAgFIAAnrQAAgFgEgEQgDgDgFAAIgKAAQgFAAgEADgAi8DNIC0AAIAAgiIi0AAgAlqBCQAHAIAKAFQAWAOAagHQAZgHAOgXQAMgVgFgXQADgOgFgQIAAAAIgDgJIAAAAQgHgSgPgLIgJALIAAAAIAEgGIgRgKIgDAEIAFgMQgWgJgYAHQgfAIgPAbIAAAAIAAAAQgPAbAHAdIAAABQADAKAGAJQAJAQASAJIgBAAIABABgAi8ASIC0AAIAAghIi0AAgAm6lZIAABXQAAAvAiAiIAAAAIAAABQAiAhAvAAIFHAAQAvAAAhgiQAigiAAgvIAAhXQAAgvgigiIAAAAQghgigvAAIlHAAQgvAAgiAiIAAAAQgiAiAAAvg");
	this.shape_9.setTransform(87.3,58.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F27A75").s().p("AsLI2IAAhSIAIAJQAcAcAoAAIV+AAQAoAAAcgcIAJgKIAABTgAMDnsIAAAAQgcgdgoAAI1+AAQgoAAgcAdIAAAAIgIAJIAAhSIYXAAIAABTIgJgKg");
	this.shape_10.setTransform(87.4,58.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,177.7,119.2);


(lib.Symbol44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFE89A").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape.setTransform(6,8.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFE69B").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_1.setTransform(6,8.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFE49D").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_2.setTransform(6,8.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFE29E").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_3.setTransform(6,8.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFE09F").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_4.setTransform(6,8.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFDEA1").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_5.setTransform(6,8.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFDCA2").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_6.setTransform(6,8.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFDAA3").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_7.setTransform(6,8.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFD8A4").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_8.setTransform(6,8.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFD6A6").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_9.setTransform(6,8.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFD4A7").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_10.setTransform(6,8.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFD2A8").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_11.setTransform(6,8.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFD1AA").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_12.setTransform(6,8.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFCFAB").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_13.setTransform(6,8.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFCDAC").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_14.setTransform(6,8.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFCBAE").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_15.setTransform(6,8.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFC9AF").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_16.setTransform(6,8.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFC7B0").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_17.setTransform(6,8.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFC5B1").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_18.setTransform(6,8.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFC3B3").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_19.setTransform(6,8.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFC1B4").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_20.setTransform(6,8.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFBFB5").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_21.setTransform(6,8.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFBDB7").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_22.setTransform(6,8.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFBBB8").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_23.setTransform(6,8.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFBFB6").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_24.setTransform(6,8.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFC0B4").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_25.setTransform(6,8.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFC2B3").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_26.setTransform(6,8.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFC4B2").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_27.setTransform(6,8.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFC6B1").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_28.setTransform(6,8.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFC8B0").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_29.setTransform(6,8.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFC9AE").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_30.setTransform(6,8.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFCBAD").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_31.setTransform(6,8.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFD8A5").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_32.setTransform(6,8.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFDAA4").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_33.setTransform(6,8.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFDBA2").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_34.setTransform(6,8.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFDDA1").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_35.setTransform(6,8.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFDFA0").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_36.setTransform(6,8.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFE19F").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_37.setTransform(6,8.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFE39E").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_38.setTransform(6,8.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFE49C").s().p("Ag7BXIAAhwQAAgZASgSQARgSAYAAQAWAAARAOQAQANAFAVQgRgOgXAAQgYAAgRARQgSASAAAYIAABQg");
	this.shape_39.setTransform(6,8.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer_2
	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF551A").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_40.setTransform(6,8.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF571E").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_41.setTransform(6,8.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF5923").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_42.setTransform(6,8.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF5C27").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_43.setTransform(6,8.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF5E2C").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_44.setTransform(6,8.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF6030").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_45.setTransform(6,8.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FF6234").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_46.setTransform(6,8.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FF6539").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_47.setTransform(6,8.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FF673D").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_48.setTransform(6,8.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FF6942").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_49.setTransform(6,8.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF6B46").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_50.setTransform(6,8.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF6D4A").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_51.setTransform(6,8.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FE704F").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_52.setTransform(6,8.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FE7253").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_53.setTransform(6,8.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FE7457").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_54.setTransform(6,8.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FE765C").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_55.setTransform(6,8.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FE7860").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_56.setTransform(6,8.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FE7B65").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_57.setTransform(6,8.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FE7D69").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_58.setTransform(6,8.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FE7F6D").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_59.setTransform(6,8.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FE8172").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_60.setTransform(6,8.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FE8476").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_61.setTransform(6,8.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FE867B").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_62.setTransform(6,8.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FE887F").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_63.setTransform(6,8.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FE8477").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_64.setTransform(6,8.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FE8273").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_65.setTransform(6,8.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FE806F").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_66.setTransform(6,8.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FE7E6B").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_67.setTransform(6,8.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FE7C67").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_68.setTransform(6,8.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FE7A63").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_69.setTransform(6,8.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FE785F").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_70.setTransform(6,8.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FE765B").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_71.setTransform(6,8.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FF673E").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_72.setTransform(6,8.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FF653A").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_73.setTransform(6,8.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FF6336").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_74.setTransform(6,8.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FF6132").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_75.setTransform(6,8.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FF5F2E").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_76.setTransform(6,8.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FF5D2A").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_77.setTransform(6,8.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FF5B26").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_78.setTransform(6,8.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FF5922").s().p("Ag7BXIAAhwQAAgZARgSQASgSAYAAQAZAAASASQARASAAAZIAABwg");
	this.shape_79.setTransform(6,8.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40}]}).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_40}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,12.1,17.4);


(lib.Symbol43 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6CBE9D").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape.setTransform(6,7.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#67C19B").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_1.setTransform(6,7.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#63C498").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_2.setTransform(6,7.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5EC696").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_3.setTransform(6,7.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#59C993").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_4.setTransform(6,7.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55CC91").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_5.setTransform(6,7.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#50CF8F").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_6.setTransform(6,7.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4BD28C").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_7.setTransform(6,7.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#46D58A").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_8.setTransform(6,7.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#42D787").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_9.setTransform(6,7.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3DDA85").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_10.setTransform(6,7.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#38DD83").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_11.setTransform(6,7.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#34E080").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_12.setTransform(6,7.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#2FE37E").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_13.setTransform(6,7.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#2AE67C").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_14.setTransform(6,7.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#26E879").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_15.setTransform(6,7.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#21EB77").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_16.setTransform(6,7.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#1CEE74").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_17.setTransform(6,7.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#17F172").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_18.setTransform(6,7.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#13F470").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_19.setTransform(6,7.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#0EF76D").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_20.setTransform(6,7.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#09F96B").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_21.setTransform(6,7.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#05FC68").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_22.setTransform(6,7.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#00FF66").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_23.setTransform(6,7.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#04FC68").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_24.setTransform(6,7.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#09FA6A").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_25.setTransform(6,7.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#0DF76D").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_26.setTransform(6,7.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#11F56F").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_27.setTransform(6,7.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#16F271").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_28.setTransform(6,7.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#1AEF73").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_29.setTransform(6,7.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#1EED75").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_30.setTransform(6,7.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#23EA78").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_31.setTransform(6,7.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#27E87A").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_32.setTransform(6,7.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#2BE57C").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_33.setTransform(6,7.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#30E27E").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_34.setTransform(6,7.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3CDB85").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_35.setTransform(6,7.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#41D887").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_36.setTransform(6,7.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#45D589").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_37.setTransform(6,7.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#49D38B").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_38.setTransform(6,7.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#4ED08E").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_39.setTransform(6,7.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#52CE90").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_40.setTransform(6,7.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#56CB92").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_41.setTransform(6,7.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#5BC894").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_42.setTransform(6,7.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#5FC696").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_43.setTransform(6,7.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#63C399").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_44.setTransform(6,7.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#68C19B").s().p("AgqBEIAAhDQAAgVAPgPQAPgPAUAAQATAAAOAMQgDgRgOgMIAEAEQAPAPAAAVIAABYIgBAHg");
	this.shape_45.setTransform(6,7.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer_1
	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#94DEA7").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_46.setTransform(5.1,7.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#98DFAA").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_47.setTransform(5.1,7.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#9CE1AE").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_48.setTransform(5.1,7.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#9FE2B1").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_49.setTransform(5.1,7.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#A3E4B4").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_50.setTransform(5.1,7.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#A7E5B8").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_51.setTransform(5.1,7.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#ABE7BB").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_52.setTransform(5.1,7.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#AFE8BE").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_53.setTransform(5.1,7.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#B3E9C1").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_54.setTransform(5.1,7.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#B6EBC5").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_55.setTransform(5.1,7.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#BAECC8").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_56.setTransform(5.1,7.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#BEEECB").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_57.setTransform(5.1,7.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#C2EFCF").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_58.setTransform(5.1,7.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#C6F1D2").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_59.setTransform(5.1,7.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#CAF2D5").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_60.setTransform(5.1,7.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#CDF4D9").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_61.setTransform(5.1,7.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#D1F5DC").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_62.setTransform(5.1,7.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#D5F6DF").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_63.setTransform(5.1,7.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#D9F8E2").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_64.setTransform(5.1,7.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#DDF9E6").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_65.setTransform(5.1,7.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#E1FBE9").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_66.setTransform(5.1,7.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#E4FCEC").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_67.setTransform(5.1,7.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#E8FEF0").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_68.setTransform(5.1,7.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#ECFFF3").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_69.setTransform(5.1,7.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#E5FCED").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_70.setTransform(5.1,7.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#E1FBEA").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_71.setTransform(5.1,7.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#DEFAE7").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_72.setTransform(5.1,7.4);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#DAF8E4").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_73.setTransform(5.1,7.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#D7F7E1").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_74.setTransform(5.1,7.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#D3F6DE").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_75.setTransform(5.1,7.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#D0F4DB").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_76.setTransform(5.1,7.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#CCF3D8").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_77.setTransform(5.1,7.4);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#C9F2D5").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_78.setTransform(5.1,7.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#C5F0D2").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_79.setTransform(5.1,7.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#BBEDC8").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_80.setTransform(5.1,7.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#B7EBC5").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_81.setTransform(5.1,7.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#B4EAC2").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_82.setTransform(5.1,7.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#B0E9BF").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_83.setTransform(5.1,7.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#ADE7BC").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_84.setTransform(5.1,7.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#A9E6B9").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_85.setTransform(5.1,7.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#A6E5B6").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_86.setTransform(5.1,7.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#A2E3B3").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_87.setTransform(5.1,7.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#9FE2B0").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_88.setTransform(5.1,7.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#9BE1AD").s().p("AgxBJIgBheQAAgVAQgPQAPgPAUgBQASAAAOAMQAOAMAEARQgOgNgTAAQgUAAgPAQQgPAPAAAVIAABCg");
	this.shape_89.setTransform(5.1,7.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_46}]}).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_46}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10.3,14.7);


(lib.Symbol41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAgA4QgWgGgGgGQgIgNAEgNIg7g7IgCABIARgWIAAADIA7A7QAQgEAMALQAGAFAGAWQADAKAEATQgNgCgRgFg");
	this.shape.setTransform(6.2,6.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,12.3,12.5);


(lib.Symbol40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgLBTIAAhYQgQgHAAgSQAAgNAbgnQAJAMAIANQALAUAAAHQAAASgQAHIAABYg");
	this.shape.setTransform(2.8,8.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,5.6,16.6);


(lib.Symbol36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D5DDEF").s().p("ADJDBQgHg2gohIQgnhEgrgpIgFgEIlUAAIACiSIBLAAIAOAAIBKACIAAAAIAfADQB8AUBhBcQB0BuAOCeg");
	this.shape.setTransform(14.4,22.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9AA0B5").s().p("ADQB4QgHg1gXgxQgag1grgoIgEgEIlVAAIAAgoIFUAAIAFAEQArApAmBEQApBIAGA2g");
	this.shape_1.setTransform(10.8,29.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#40516C").s().p("ACADfIgCgNQgEg4gYgyQgWgugkgjIlKAAIAAgdIABAAIFVAAIAEAEQArAoAaA1QAYAxAGA1IAeAAIBJAAQgOieh0huQhhhch8gUIgfgDIAAAAIhKgCIgOAAIhLAAIAAgeIBLAAIAXAAIBBABIAjAFQCGAVBnBjIAAgBQCDB8AKC1IACAPg");
	this.shape_2.setTransform(16,22.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol36, new cjs.Rectangle(-13,0,58.2,44.7), null);


(lib.Symbol35 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("AkfDfIABgQQAKi0CDh8IAAABQB3hyCfgLIAFAAIAAAAIAYAAIAAgBIAFAAIAAABIB3gBIACAAIAAAeIgCAAIiUABQiXAJhvBrIAAAAQh1BugOCeIBnAAQAHg1AXgxQAag1ArgoIAEgEIC6AAIAAAdIiuAAQgkAjgWAuQgYAygEA4IgBANgACJAXIAAgdICUAAIAAAdg");
	this.shape.setTransform(27.2,20.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9AA0B5").s().p("AjpB4QAHg3AohGQAnhGArgoIAEgEIFOAAIAAAoIiUAAIAAAAIi6AAIgEAEQgpAngaAyQgZAxgHA5g");
	this.shape_1.setTransform(32.4,28.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E8EEF9").s().p("AjdDBQAPikB6huQBuhlCSgJIAAABIgFAAIAAACQhwAbh1BrQhyBlgjCSgACui/IAXgBIACAAIAIAAIAPAAIgwACg");
	this.shape_2.setTransform(23.8,20.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5DDEF").s().p("AkXDQQAJiuB/h4QBzhtCZgLIAAAAIAFAAIAYgBIAAAAIAFAAIAAAAIB3AAIACAAIgCChIgBAAIlOAAIgEAEQgrApgnBEQgoBIgHA2IAeAAQAHg4AZgyQAagzApgmIAEgEIC6AAIAAAOIi0AAQgpAngYAzQgaA0gEA6gACCjAIAAABQiSAJhvBlQh5BugPCkIAKAAQAiiSByhlQB1hrBxgbIAAgBIAFgBIACAAIAvgCIgOAAIgIAAIgDAAIgYAAgACBAIIAAgOICUAAIAAAOg");
	this.shape_3.setTransform(28,20.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.6,-1.5,57.7,44.7);


(lib.Symbol32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("AhnG/IAAgaIpQAAIAAkiIgcAAIAAkYIAcAAIAAkLIJQAAIAAgeIDwAAIAAAeIIwAAIAAELIAbAAIAAEYIgbAAIAAEiIowAAIAAAagAhJELIAACWIC0AAIAAi/gAHVCSIlMBKIAACrIISAAIAAkEIjJAAgAqZGHIIyAAIAAh2Ii+ApIgpi3IlLAAgAhVi9IgBAAIkSA9IBaGXILAicIgBgDIAAgBIg3j5IgBgBIgiiZIjaAwIAAAAgAHMBlIDqAAIAAjcIkbAAgAq1BlIFgAAIgwjcIkwAAgAGViVIEGAAIAAjtIoSAAIAAB0IDlgygAqZiVIENAAIgBgCIEmhBIAAiqIoyAAgAhJjfIC0goIAAiZIi0AAg");
	this.shape.setTransform(70.9,43.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF2A00").s().p("AjFgiIAfgJIAkCCIAqgMIAIAfIhJAUgAhug5IBKgVIAIAeIgqAMIAJAiIAlgKIAJAdIglAKIAKAkIAqgLIAHAeIhJAVgAABhYIAmgLIBLCYIghAJIgNgeIgYAGIAEAhIgiAJgAAfg0IAJA8IAPgEIgWg4gABUhvIArgMQAOgDAKACQAKADAGALQAEAHAFAPIAKAjIAJAjQAEAQgBAGQgBAPgIAIQgIAIgMADIgpALgACBhbIgFABIAcBkIAEgBQAIgCABgEQABgDgCgHIgUhJQgBgHgDgCQgCgDgEAAIgFABg");
	this.shape_1.setTransform(73.5,43.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AmNh9IETg9IABAAIDRgvIABAAIDagwIAiCZIAAABIA4D5IAAABIABADIrBCcgAjQgdIAsCgIBJgVIgIgeIgqALIgkiBgAh5g1IAsCgIBKgUIgIgfIgqAMIgKgkIAlgKIgJgeIglAJIgJggIAqgMIgIgegAgJhTIAMCoIAigJIgEghIAYgHIANAfIAhgJIhLiYgAB0h2IgrALIAsCgIApgKQAMgDAIgIQAIgIABgPQABgIgEgOIgJgjIgKgjQgFgPgEgIQgGgKgKgDIgJgBQgHAAgIACgABxhVIAFgBQAHgCAEAEQADACABAHIAUBJQACAFgBAEQgBAFgIACIgEABgAAUgvIACAAIAWA3IgPAFg");
	this.shape_2.setTransform(74.6,42.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD66B").s().p("AhJGhIAAiWIC0gpIAAC/gAHMBlIgxjcIEbAAIAADcgAq1BlIAAjcIEwAAIAwDcgAhJmgIC0AAIAACZIi0Aog");
	this.shape_3.setTransform(70.9,43.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FBBB73").s().p("ACIGFIAAirIFNhJIgEgQIDJAAIAAEEgAqaGFIAAkEIFLAAIApC4IC+gqIAAB2gAGUiXIgnirIjlAzIAAh1IISAAIAADtgAqaiXIAAjtIIyAAIAACqIkmBBIABACg");
	this.shape_4.setTransform(71,43.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol32, new cjs.Rectangle(-1.5,-1.5,144.9,89.5), null);


(lib.Symbol31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8EEF9").s().p("AA3AmIh4gvQgIgDgEgGQgDgHACgGQADgFAGgCQAHgCAIADIB4AwQAHADAEAGQAEAGgCAGQgCAGgIACIgFAAQgEAAgFgCg");
	this.shape.setTransform(25.5,34.9,1,1,-22.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8EEF9").s().p("AAIAUIgbgMQgHgDgEgFQgEgGACgGQADgGAHgCQAHgCAHAEIAbALQAIADAEAFQAEAHgDAFQgCAGgHACIgGABQgEAAgFgCg");
	this.shape_1.setTransform(10.5,35,1,1,-22.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E8EEF9").s().p("ABsBVQAAg6gogoQgqgpg4AAQggAAgdANQgbANgUAXQALgwAmgeQAmgfAyAAQA6AAAoApQApApABA5QgBAyghAoQADgRAAgNg");
	this.shape_2.setTransform(100.6,39,1,1,-22.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#40516C").ss(3).p("ACbAAQAABAguAtQgtAuhAAAQg/AAgtguQgugtAAhAQAAg/AugtQAtguA/AAQBAAAAtAuQAuAtAAA/g");
	this.shape_3.setTransform(101.4,41.3,1,1,-22.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D5DDEF").s().p("AhsBuQguguAAhAQAAg/AuguQAsgtBAAAQBAAAAuAtQAtAuAAA/QAABAgtAuQguAthAAAQhAAAgsgtg");
	this.shape_4.setTransform(101.4,41.3,1,1,-22.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CCD3E2").s().p("AC/A7QAAhmhKhJQhJhJhmgBQhbAAhHA7QAkgqAwgWQAzgZA5AAQBmAABJBKQBKBJAABnQAAA3gYA0QgYAwgpAjQA7hGAAhbg");
	this.shape_5.setTransform(103,37.5,1,1,-22.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#40516C").ss(3).p("AEJAAQAABthNBPQhPBNhtAAQhtAAhOhNQhNhPAAhtQAAhtBNhOQBOhNBtAAQBtAABPBNQBNBOAABtg");
	this.shape_6.setTransform(101.4,41.3,1,1,-22.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B4B8C7").s().p("Ai6C7QhOhOAAhtQAAhtBOhNQBNhOBtAAQBtAABPBOQBNBNAABtQAABthNBOQhPBOhtAAQhtAAhNhOg");
	this.shape_7.setTransform(101.4,41.3,1,1,-22.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#40516C").ss(3).p("AkIB9QAshwBchBQBahBBnADQBJACA/BVQAgAsAgA6");
	this.shape_8.setTransform(80.3,18.7,1,1,-22.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#40516C").ss(3).p("Aj0CkQAWibBthaQBnhWBpAWQBRARAqBpQAVA0AGA3");
	this.shape_9.setTransform(67.8,17.4,1,1,-22.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#40516C").ss(3).p("ACDBnIknh5IAihUIEnB5g");
	this.shape_10.setTransform(55,40.5,1,1,-22.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E8EEF9").s().p("AikgSIAihUIEnB5IgiBUg");
	this.shape_11.setTransform(55,40.5,1,1,-22.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#8E94A3").s().p("AGKD+IstlOQgNgFgHgLQgGgLAEgKIA0iAQAEgHAJgEIgtBtQgEAKAGALQAHALANAFIMtFOQAQAGAOgGIgHATQgEAKgNADIgKABQgIAAgIgDg");
	this.shape_12.setTransform(46.9,41.4,1,1,-22.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#40516C").ss(3).p("AGrD6QgGAQgSAFQgTAGgTgIIstlNQgTgIgJgRQgJgQAGgQIA1iAQAHgQASgFQASgGATAIIMtFNQATAIAJARQAJARgGAPg");
	this.shape_13.setTransform(49.1,40.4,1,1,-22.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#B4B8C7").s().p("AFtENIstlNQgTgIgJgRQgJgQAGgQIA1iAQAHgQASgFQASgGATAIIMtFNQATAIAJARQAJARgGAPIg1CAQgGAQgSAFQgIACgHAAQgMAAgLgEg");
	this.shape_14.setTransform(49.1,40.4,1,1,-22.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#40516C").ss(3).p("AGrD6QgGAQgSAFQgTAGgTgIIstlNQgTgIgJgRQgJgQAGgQIA1iAQAHgQASgFQASgGATAIIMtFNQATAIAJARQAJARgGAPg");
	this.shape_15.setTransform(49.1,40.4,1,1,-22.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#B4B8C7").s().p("AFtENIstlNQgTgIgJgRQgJgQAGgQIA1iAQAHgQASgFQASgGATAIIMtFNQATAIAJARQAJARgGAPIg1CAQgGAQgSAFQgIACgHAAQgMAAgLgEg");
	this.shape_16.setTransform(49.1,40.4,1,1,-22.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol31, new cjs.Rectangle(-1.5,-4,134.5,76.9), null);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8EEF9").s().p("Anvi5IAKgeIPWGSIgMAdg");
	this.shape.setTransform(109.9,3.1,1.895,1.197,0,-39.3,-11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#40516C").ss(3).p("AHjEHIvymeIAthvIPyGeg");
	this.shape_1.setTransform(109.8,6.1,1.895,1.197,0,-39.3,-11.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5DDEF").s().p("AoPiXIAthvIPyGeIgtBvg");
	this.shape_2.setTransform(109.8,6.1,1.895,1.197,0,-39.3,-11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol30, new cjs.Rectangle(-1.3,-5.9,222.2,24.1), null);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8EEF9").s().p("AgOEfIAAo9IAdAAIAAI9g");
	this.shape.setTransform(3,104.2,1,3.445);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#40516C").ss(3).p("AA3EvIhtAAIAApdIBtAAg");
	this.shape_1.setTransform(5.5,104.2,1,3.445);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5DDEF").s().p("Ag2EuIAApbIBtAAIAAJbg");
	this.shape_2.setTransform(5.5,104.2,1,3.445);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol29, new cjs.Rectangle(-1.5,-1.5,14,211.5), null);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").ss(3).p("ABoAAQAAArgeAeQgfAfgrAAQgqAAgegfQgfgeAAgrQAAgqAfgeQAegfAqAAQArAAAfAfQAeAeAAAqg");
	this.shape.setTransform(17.8,17.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8EEF9").s().p("ABCACQgdgXglAAQgkAAgeAXQgcAYgHAkIgCgUQAAgqAfgeQAegfAqAAQArAAAfAfQAeAeAAAqIgCAUQgHgkgdgYg");
	this.shape_1.setTransform(17.8,13.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D5DDEF").s().p("ABhAZQgsgeg1AAQg1AAgrAeQgpAggQAxQgJgZABgZQgBhCAwgwQAwgvBCAAQBDAAAwAvQAwAwAABCQAAAZgJAZQgPgxgqggg");
	this.shape_2.setTransform(17.8,12.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#40516C").ss(3).p("ACyAAQAABKg0A0Qg0A0hKAAQhIAAg1g0Qg0g0AAhKQAAhJA0g0QA1g0BIAAQBKAAA0A0QA0A0AABJg");
	this.shape_3.setTransform(17.8,17.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B4B8C7").s().p("Ah8B+Qg1g0AAhKQAAhJA1gzQA0g1BIAAQBKAAA0A1QA0AzAABJQAABKg0A0Qg0A0hKgBQhIABg0g0g");
	this.shape_4.setTransform(17.8,17.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(-1.5,-6.4,38.5,43.4), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#94DEA8").s().p("AgcD8QgLgCgGgOQgGgPACgSIAwmfQABgSAJgMQAJgLALACQALACAGAOQAGAPgCASIgwGfQgCASgIAMQgIAKgIAAIgEgBg");
	this.shape.setTransform(5.1,25.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10.1,50.6);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").ss(3).p("AFZBBIqxAAIAAiBIKxAAg");
	this.shape.setTransform(88,20.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DC8E53").s().p("AlYBBIAAiBIKxAAIAACBg");
	this.shape_1.setTransform(88,20.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#40516C").ss(3).p("ANwDNI7fAAIAAmZIbfAAg");
	this.shape_2.setTransform(88,20.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FBBB73").s().p("AtvDNIAAmZIbfAAIAAGZg");
	this.shape_3.setTransform(88,20.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DC8E53").s().p("ArrCFIAAkJIXWAAIAAEJg");
	this.shape_4.setTransform(88,43.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#40516C").ss(3).p("AL4F3I3vAAIAArtIXvAAg");
	this.shape_5.setTransform(88,57.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FBBB73").s().p("Ar3F3IAArtIXvAAIAALtg");
	this.shape_6.setTransform(88,57.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,179,98.3);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF8B9").s().p("Ag+BPQgcghAAguQAAgtAcghQAagfAkgBQAlABAaAfIAAAAQAcAhAAAtQAAAugcAhIAAAAQgaAfglABQgkgBgagfgAgfAjQAOAFAOAAIAAANIAHAAIAAgNQANgCAIgGQAHgFAAgLQAAgFgDgFQgCgDgFgDQgEgDgOgFIAAgTQAIAAANAFIAFgPQgMgEgOgBIAAgKIgHAAIAAAKQgNABgHAGQgIAGAAAJQAAAIAGAHQAGAFALAEIAFABIAAAUIgOgCIgOgFgAAEAJIAHAEQAAAAABABQAAAAAAABQAAAAABABQAAABAAABQAAAGgJABgAgKgPQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQAAgGAJAAIAAAOg");
	this.shape.setTransform(33,20.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6CFFAE").s().p("AlCCvIAAAAIgBAAQgFgGAAgIIAAkoQAAgHAFgFIABAAIAAgBQADgEAFgBIABAAIAAAAIABAAIACAAIJSAAQAIAAAFAGQAFAFAAAHIAAEoIAAAEQgBAGgEAEQgFAFgIAAIpSAAIAAAAQgHAAgFgFgADhBqQgYAWAAAdQAAAGACAIIBRAAIAAhVIgDAAQghgBgXAVgAlABWIAABVIBRAAQACgGAAgIQAAgdgXgWQgYgVggABgAhjhVQgiApAAA5QAAA6AiApQAkAqAzgBQAyABAjgqIAAAAQAigpAAg6QAAg5gigpIAAABQgjgrgyAAIgBAAQgyAAgkAqgADJiCQAAAeAYAUQAXAWAhAAIADgBIAAhWIhRAAQgCAKAAAFgAlAg7IAEABQAgAAAYgWQAXgUAAgeQAAgHgCgIIhRAAgAE+ipQgHgGgHgEIAHAAQAHAAAGAGIAAgBQAFAFAAAIIAAAIQgEgJgHgHg");
	this.shape_1.setTransform(34.4,19.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#40516C").s().p("AEcDSIpSAAQgUAAgOgOIAAgBQgOgNAAgVIAAkoQAAgUAOgOIAAABQAHgIAJgEQAEgIAHgHQAOgOAUAAIJSAAQATAAAOAOIABAAIAAAAQAOAOAAAUIAAEoQAAAUgOAOIAAAAIAAAAQgHAIgJADQgEAJgHAHQgOAOgUAAIAAAAgAlIiHIAAEoQAAAIAFAGIAAAAIABAAQAFAFAHAAIJSAAQAIAAAFgFQAEgEABgGIAAgEIAAkoQAAgHgFgFQgFgGgIAAIpSAAIgCAAIgBAAIgBAAIAAAAQgFABgDAEIgBABIAAAAQgFAFAAAHgAE+ipQAHAHAEAJIAAgIQAAgIgFgFIAAABQgGgGgHAAIgHAAQAHAEAHAGgADLCrQgCgIAAgGQAAgdAYgWQAXgVAhABIADAAIAABVgAlACrIAAhVIAEAAQAggBAYAVQAXAWAAAdQAAAIgCAGgAhjBwQgigpAAg6QAAg5AigpQAkgqAzAAQAyAAAjArIAAgBQAiApAAA5QAAA6giApIAAAAQgjAqgygBIgBAAQgyABgkgqgAhMhBQgbAhAAAtQAAAuAbAhQAbAfAlABQAkgBAagfIAAAAQAbghAAguQAAgtgbghIAAAAQgagfgkgBQglABgbAfgAgRBCIAAgNQgOAAgNgFIAAgRIANAFIAOACIAAgUIgEgBQgLgEgHgGQgFgHAAgHQAAgJAHgGQAHgGANgBIAAgKIAJAAIAAAKQANABAMAEIgGAPQgLgFgIAAIAAASQAMAFAEADQAGAEACADQACAFAAAFQAAALgHAFQgHAGgMACIAAANgAgIAmQAIgBAAgGQAAgBAAgBQAAgBAAAAQgBgBAAAAQAAgBgBAAIgGgEgAgZgHQAAABAAAAQAAABAAAAQABABAAAAQAAABABABIAGACIAAgNQgIAAAAAGgADhhQQgYgUAAgeQAAgFACgKIBRAAIAABWIgDABQghAAgXgWgAlAg7IAAhWIBRAAQACAIAAAHQAAAegXAUQgYAWggAAg");
	this.shape_2.setTransform(34.4,19.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,71.8,43.7);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgOAOQgFgFAAgJQAAgHAFgHQAHgFAHAAQAJAAAFAFQAHAHgBAHQABAJgHAFQgFAHgJgBQgHABgHgHg");
	this.shape.setTransform(4.2,9.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#40516C").s().p("AgdA4QgNgLAAgSQAAgRANgLQALgMASgBIAIABIAUgyQABgDAEgCIADAAIACAAIAFAFQABACgBADIgWAzIAEACQAPAMAAAUQAAASgMALQgMANgQAAQgRAAgMgNg");
	this.shape_1.setTransform(4.3,6.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,8.6,13.8);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXAYQgKgKAAgOQAAgNAKgKQAKgKANAAQAOAAAKAKQAKAKAAANQAAAOgKAKQgKAKgOAAQgNAAgKgKg");
	this.shape.setTransform(7.3,15.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#40516C").s().p("AgtBdQgVgUAAgcQAAghAagTIAGgFIgjhTQgBgEABgFQADgFAEgCIAFgBIAFABQAFACABAFIAjBSIANgBQAcAAAVAUQAUATAAAdQAAAcgUAUQgVAUgcAAQgbAAgUgUg");
	this.shape_1.setTransform(7.1,11.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,14.2,22.7);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").p("ABlm5QgHgCgHAAQgRAAgOAJQgVALgIAWQgFgVgQgOQgSgQgbACIgBAAQgFABgFABQgMADgJAHIgDACQgEAEgFAEQgPARABAXQgCgDgCgCIgCgDQgOgSgWgGQgVgDgRAHQgVAJgJAVQgCADgCADQgJAUAHAVQgOgNgNgCQgOgDgPADQgDABgDACQgNAEgJAIQgSAQgDAZQgCAYANASIgLgDQgGgCgGAAIgEAAQgCAAgCAAQgJABgIADQgUAIgKARQgBABAAAAIgCAGQgJASADAUQACAIADAHQAFAMALAJQgSgCgTAMIgJAGQgNAMgGAXQgFAYALAVQAKAUAVAIQgRADgMAMQgMAMgFASIgCAUQAEAZASASQASAOASABQACAAACAAQgBABgBABQgNAHgIANQgGAJgDAKQgBAIAAAHQAAADAAACIAFATQABABAAABQAJASARAKQADACAEACQACABADABQAIACAIABQAKAAAJgCQgMALgCAQQgEAOAFAPQABAFACAEQAEAKAHAJQAPATAcADQAUADATgOIgBADQgHAOADAOQABAOAIANQACADACADIACACQAHAIAIAFQACABACABIAKAEQAPAFARgDQAXgFAOgTQgBAMADAMQADAOAKAMQANAPAUAFQAVAFATgJQADgCADgCQAUgLAIgWQAFAWAQAOQARAPAXAAIAFAAQAZgCARgSQALgNAEgQQABgGAAgHQAJAQAQAJQAQAIAUgBIATgFQACgBACgBQAUgJAKgYQAJgSgHgXQAIAHAJAEQAHAEAGABQAOADAPgEQAOgEAMgKQAEgDADgEQAMgOACgWQACgOgGgOQgDgGgEgGQAUAIANgCQAOgBANgIQANgIAIgNQAJgQAAgSQAAgIgCgIQgGgUgRgNIANAAQAVgDAPgNQAQgOAEgUQACgNgCgLQgCgOgHgKQgLgSgTgIQAUgGAOgPQAQgSgDgbQgCgVgMgOQgEgFgFgFQgRgOgTAAQAOgJAHgPQADgGACgGQABgDABgDQAAgDABgCIAAgNQgBgKgEgKQgIgXgZgLQgSgKgYAIQAAAAAAgBIABAAQAJgKAEgLIAAgBQACgMgBgMQgDgVgMgPQgPgTgcgDQgDAAgDAAIAAgBQgSABgQAKQAGgMABgLIgBgBQABgNgFgOQgHgSgSgLQgWgNgXAGQgGABgFADIAAgBQgQAGgKANQAAgWgKgSQgMgTgWgGQgDgBgDgBg");
	this.shape.setTransform(45,45.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#40516C").s().p("AAhHDQgXAAgRgPQgQgOgFgWQgIAWgUALIgGAEQgTAJgVgFQgUgFgNgPQgKgMgDgOQgDgMABgMQgOATgXAFQgRADgPgFIgKgEIgEgCQgIgFgHgIIgCgCIgEgGQgIgNgBgOQgDgOAHgOIABgDQgTAOgUgDQgcgDgPgTQgHgJgEgKIgDgJQgFgPAEgOQACgQAMgLQgJACgKAAQgIgBgIgCIgFgCIgHgEQgRgKgJgSIgBgCIgFgTIAAgFQAAgHABgIQADgKAGgJQAIgNANgHIACgCIgEAAQgSgBgSgOQgSgSgEgZIACgUQAFgSAMgMQAMgMARgDQgVgIgKgUQgLgVAFgYQAGgXANgMIAJgGQATgMASACQgLgJgFgMQgDgHgCgIQgDgUAJgSIACgGIABgBQAKgRAUgIQAIgDAJgBIAEAAIAEAAQAGAAAGACIALADQgNgSACgYQADgZASgQQAJgIANgEIAGgDQAPgDAOADQANACAOANQgHgVAJgUIAEgGQAJgVAVgJQARgHAVADQAWAGAOASIACADIAEAFQgBgXAPgRIAJgIIADgCQAJgHAMgDIAKgCIABAAQAbgCASAQQAQAOAFAVQAIgWAVgLQAOgJARAAIAOACIAGACQAWAGAMATQAKASAAAWQAKgNAQgGIAAABIALgEQAXgGAWANQASALAHASQAFAOgBANIABABQgBALgGAMQAQgKASgBIAAABIAGAAQAcADAPATQAMAPADAVQABAMgCAMIAAABQgEALgJAKIgBAAIAAABQAYgIASAKQAZALAIAXQAEAKABAKIAAANIgBAFIgCAGIgFAMQgHAPgOAJQATAAARAOQAFAFAEAFQAMAOACAVQADAbgQASQgOAPgUAGQATAIALASQAHAKACAOQACALgCANQgEAUgQAOQgPANgVADIgNAAQARANAGAUQACAIAAAIQAAASgJAQQgIANgNAIQgNAIgOABQgNACgUgIIAHAMQAGAOgCAOQgCAWgMAOIgHAHQgMAKgOAEQgPAEgOgDQgGgBgHgEQgJgEgIgHQAHAXgJASQgKAYgUAJIgEACIgTAFQgUABgQgIQgQgJgJgQIgBANQgEAQgLANQgRASgZACg");
	this.shape_1.setTransform(45,45.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1.6,92.7,93.8);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAMGhQgKgIgDgSIgBgLQAAgGgEgDQgDgEgFAAIgTgBQgLAAgDAKIgBAKQgEAQgNAIQgOAJgPgEQgbgJgCgZQgEgFAEgGIACgLQABgFgCgFQgDgEgFgBIgMgFIgGgCQgKgDgGAIIgFALQgHANgQAEQgPADgNgHQgQgLgCgSQAAgMAFgKIAFgKQACgEgBgFQgBgEgEgCIgNgNQgJgFgIAFIgJAHQgJAJgSAAQgPgDgKgKQgJgMAAgLQAAgTAOgNIAIgHQAIgHgGgKIgKgPQgDgEgEgBQgFgBgEABIgKAFQgPAFgOgFQgPgHgFgNQgDgIAAgEQgCgMAGgKQAGgLAMgFIAKgDQAJgFgCgJIgFgQQgDgLgLAAIgKACQgOACgNgKQgLgJgDgRQgCgOAKgNQAJgMARgBIAKgCQAFABAEgEQADgEAAgEIACgSQAAgLgKgCIgLgCQgPgEgJgNQgIgOADgPQAKgbAYgCIAHAAIAFAAIALACQAFACAEgDQAFgDABgFIAHgRQACgDgCgFQgBgEgEgCIgJgEQgPgJgCgPQgFgPAIgOQALgPASgEQALAAALAGIALAFQAEACAEAAQAEgCADgEIAMgNQAFgJgFgIIgHgIQgKgMABgPQABgQALgKQAMgJAKAAQATAAAOAOIAHAIQAHAIAJgGIAQgKQADgCACgFQABgEgBgEIgFgKQgGgRAGgNQAGgOAOgGQAIgEAEABQAMgBAKAFQAKAGAFALIAEALQAGAJAJgDIARgEQAKgDAAgLIAFgMQgBgPAKgMQAKgNAPgBQAOgCAOALQAJAIADARIACAKQAAAGADADQAEADAFAAIASAAQAMABACgLIACgKQAEgPANgJQAOgJAPAEQALADAIAJQAIAJABANIABAGIgBAGIgBAKQgBAFACAFQACAEAEACIABAAIAQAHQAJACAGgIIAFgKQAIgOAPgEQAPgDANAHQAQAKADAUQAAAKgGALIgFAKQgFAMAJAFIANAMQAJAHAIgHIAJgGQAMgJAPAAQAPADAKAKQAJALAAAMQADARgPAPIgIAHQgJAIAHAJIAKAPQAFAIAKgDIALgDQANgFAPAFQAPAGAGAOQADAGAAAGQABAMgGAKQgGAKgLAGIgKADQgJAGACAJQAAAHAEAIIAAACQACALALAAIAKgDQAOgCAOALQAKAJADAQQADAQgLAMQgJAKgRADIgKABQgFAAgDADQgEAEAAAFIgCARQAAANALADIAKABQARAGAHALQAHAMgCARQgDALgJAIQgKAJgMAAIgHABIgFgBIgKgBQgGgBgEACQgFADgBAEIgHARQgDALAJAFIAKAFQAOAHADAPQAEAOgIAPQgKAQgTACQgMABgKgGIgKgEQgLgGgGAIQgIAJgEAGQgFAHAFAKIAHAIQAJALAAAQQgDAOgLALQgMAJgKAAQgTAAgNgNIgHgJQgIgHgJAFIgPALQgJAFADAKIAEAKQAFANgFAQQgGAOgOAGQgGAEgGAAQgMAAgKgGQgLgFgFgMIgDgKQgDgIgJAAIgDAAIgRAFQgLACAAALIACAKQABAQgKAMQgKAMgPACIgGAAQgMAAgJgJg");
	this.shape.setTransform(42.9,42.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85.8,85.2);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAUEHQACgVgNgPQgMgQgUgCQgWgDgQANQgQAMgDAWIAAAEQgygNgugnIADgEQARgMACgVQACgTgMgRQgNgQgUgDQgVgDgQANIgEAEQgZgwgFg6IAFAAQAUADAQgNQAQgMACgVQADgVgNgQQgNgQgVgDIgFAAQAOgyAngvIADAEQANAQAUADQAUACARgNQAQgMADgUQACgVgNgQIgDgEQAygaA3gEIAAAFQgCAXAMAOQAMAQAVADQAVACARgKQAQgPACgUIAAgFQAzAOAuAnIgBABQgRANgCAUQgDAVANAQQANARAUACQAUADARgNIAFgCQAZAwAFA6IgFAAQgVgEgQAOQgQAMgDAVQgCAVAMAQQAOAQAUADIAFAAQgOAygmAvIgEgEQgMgRgVgCQgUgDgQANQgRANgCAVQgDATANARIACAAQgxAag4AEg");
	this.shape.setTransform(26.9,26.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,53.9,53.5);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").p("Aj6CcQAHAFACAAQAGABAEgDIAOgNQAJgGAKABQAKABAGAIQAGAIgBAKQgBAKgHAHIgPAMQgBAAAAACQgCACAAACQgBACgBABQgBAEAFAFQA6A2BKASQACAAACAAQADAAAGgCQAFgEAAgFIACgSQABgLAIgGQAIgHAKABQAKACAHAIQAEAGgBAJIgCATQgCAGAEAEQACADADACQADABADAAQBQgDBBgoQAFgFAAgDQAAgCAAgBQAAgEgDgDIgNgQQgDgFgBgFQgBgDAAgEQABgKAIgHQAIgIAKACQAKADAHAHIAMAPQABABACABQACABACABQABAAACABQAEAAADgDQACgBAAgBQA3g6AShKQACgEgEgHQgCgFgGAAQgBAAAAAAIgUgCQgJgCgFgGQgHgIABgLQAAgLAJgGQAIgFAKAAIATACQACAAAIgDQAEgDAAgFQAAgCgDgJQgDhIgng/QAAgCgIgEQgFgBgEAEIgPAMQgJAIgJgDQgJgBgIgIQgDgDgCgEQgBgFAAgFIAAgBQACgJAIgIIAOgLQAFgCAAgGQABgCAAgCQgCgHgCgCQg7g3hKgSQgIABgBACQgBAAgBACQgEADAAAFIgCATQAAABAAACQgCAJgIAFQgHAFgKAAQgFAAgFgCQgDgCgDgDQAAgBgBgBQgFgIAAgLIACgSQAAgHgEgFQgGgEgDAAQhPADhBAoQgCABAAACQgEACgCAHQgCAFAEAEIAMAOQAIAIgCALQgBAIgJAIQgDADgEACQgEAAgDAAQgKgBgHgIIgOgPQgCgFgGAAQgEAAgDADQgEACgCABQg2A6gSBKQABAIACACQABABACABQABAEAHAAIASACQAEAAAEABQAFADAEAFQACACABACQACAGAAAIQAAADAAACQgDAHgGAFQgIAFgKAAIgTgCQgGgBgEADQgGAFABAEQABAMADAPQAHBBAhA3g");
	this.shape.setTransform(29.5,29.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#40516C").s().p("AADEmIgFgFQgEgEACgGIACgTQABgJgEgGQgHgIgKgCQgKgBgIAHQgIAGgBALIgCASQAAAFgFAEQgGACgDAAIgEAAQhKgSg6g2QgFgFABgEIACgDQAAAAAAgBQAAgBAAAAQABgBAAAAQAAgBABAAIABgCIAPgMQAHgHABgKQABgKgGgIQgGgIgKgBQgKgBgJAGIgOANQgEADgGgBQgCAAgHgFQghg3gHhBQgDgPgBgMQgBgEAGgFQAEgDAGABIATACQAKAAAIgFQAGgFADgHIAAgFQAAgIgCgGIgDgEQgEgFgFgDIgIgBIgSgCQgHAAgBgEIgDgCQgCgCgBgIQAShKA2g6IAGgDQADgDAEAAQAGAAACAFIAOAPQAHAIAKABIAHAAQAEgCADgDQAJgIABgIQACgLgIgIIgMgOQgEgEACgFQACgHAEgCIACgDQBBgoBPgDQADAAAGAEQAEAFAAAHIgCASQAAALAFAIIABACIAGAFQAFACAFAAQAKAAAHgFQAIgFACgJIAAgDIACgTQAAgFAEgDIACgCQABgCAIgBQBKASA7A3QACACACAHIgBAEQAAAGgFACIgOALQgIAIgCAJIAAABQAAAFABAFQACAEADADQAIAIAJABQAJADAJgIIAPgMQAEgEAFABQAIAEAAACQAnA/ADBIIADALQAAAFgEADIgKADIgTgCQgKAAgIAFQgJAGAAALQgBALAHAIQAFAGAJACIAUACIABAAQAGAAACAFQAEAHgCAEQgSBKg3A6IgCACQgDADgEAAIgDgBIgEgCIgDgCIgMgPQgHgHgKgDQgKgCgIAIQgIAHgBAKIABAHQABAFADAFIANAQQADADAAAEIAAADQAAADgFAFQhBAohQADIgGgBg");
	this.shape_1.setTransform(29.5,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-2.6,61,63.1);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("Ap5AMIAAgXITzAAIAAAXg");
	this.shape.setTransform(63.4,1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,126.8,2.4);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("AEXA/QgHAAgGgFQgFgFAAgHIAAgcIyyAAQgHAAgFgFQgFgGAAgHQAAgGAFgFQAFgFAHAAISyAAIAAgcQAAgHAFgGQAGgFAGAAIBcAAQAHAAAFAFQAFAGAAAHIAAAcIIrAAQAHAAAFAFQAFAFAAAGQAAAHgFAGQgFAFgHAAIorAAIAAAcQAAAHgFAFQgFAFgHAAg");
	this.shape.setTransform(95.9,12.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,191.9,19);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("AgtFVQgHgBgGgFQgEgFAAgGIAAhEQgngJgigVIgwAvQgEAGgIAAQgHAAgFgGIhChBQgEgEAAgIQAAgHAEgGIAwgvQgUghgKgnIhDAAQgHAAgFgFQgGgGAAgGIAAhcQAAgHAGgGQAFgEAHAAIBDAAQAKgnAUgiIgwgwQgEgEAAgIQAAgGAEgGIBChBQAFgFAHAAQAIAAAEAFIAwAwQAigVAngKIAAhDQAAgHAEgFQAGgGAHAAIBbAAQAHAAAFAGQAGAFAAAHIAABDQAnAKAhAVIAvgwQAGgFAHAAQAIAAAEAFIBBBBQAGAGAAAGQAAAIgGAEIgvAwQAVAiAIAnIBEAAQAHAAAFAEQAFAGABAHIAABcQgBAGgFAGQgFAFgHAAIhEAAQgIAmgVAiIAvAvQAGAGAAAHQAAAIgGAEIhBBBQgEAGgIAAQgHAAgGgGIgvgvQghAUgnAKIAABEQAAAGgGAFQgFAFgHABg");
	this.shape.setTransform(34.1,34.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,68.1,68.1);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgcEyIAAhAQAAgHgEgEQgEgFgGgBQgvgJgngbQgFgDgGAAQgHABgDAEIguAtIgpgoIAtgtQAFgFAAgGQABgHgDgFQgbgngIguQgCgGgFgEQgFgEgFAAIhAAAIAAg5IBAAAQAFAAAFgEQAFgEACgGQAHguAcgoQADgFgBgGQAAgGgFgFIgtgtIApgpIAuAuQADAEAHABQAGAAAFgDQAogbAugJQAOgCAAgPIAAhAIA5AAIAABAQAAAHADAEQAEAFAHABQAuAJAnAbQANAIAJgKIAuguIAoApIgtAtQgEAFgBAGQgBAGAEAFQAaAmAKAwQABAGAEAEQAGAEAGAAIBAAAIAAA5IhAAAQgGAAgGAEQgEAEgBAGQgKAwgaAlQgEAGABAGQABAGAEAFIAtAtIgoAoIgugtQgKgKgMAIQgnAbguAJQgHABgEAFQgDAEAAAHIAABAg");
	this.shape.setTransform(30.6,30.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,61.1,61.2);


(lib.Design_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AuOCgQhAAAgugtQgtguAAhAIAAgJQAAhAAtguQAugtBAAAIcdAAQBAAAAtAtQAuAuAABAIAAAJQAABAguAuQgtAthAAAg");
	this.shape.setTransform(916.4,20.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AqTCgQhBAAgtgtQgtguAAhAIAAgJQAAhAAtguQAtgtBBAAIUnAAQBBAAAtAtQAtAuAABAIAAAJQAABAgtAuQgtAthBAAg");
	this.shape_1.setTransform(81.5,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AzeCgQhAAAgtgtQgtguAAhAIAAgKQAAg/AtguQAtgtBAAAMAm9AAAQBAAAAtAtQAtAuAAA/IAAAKQAABAgtAuQgtAthAAAg");
	this.shape_2.setTransform(399.1,69.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("EgnNACbQhAAAgtguQgtgtAAhAQAAg/AtgtQAuguA/AAMBObAAAQBAAAAtAuQAtAtAAA/QAABAgtAtQgtAuhAAAg");
	this.shape_3.setTransform(480.6,18.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1023,85.3);


(lib.Symbol42 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#527A68").s().p("AihAfIAAgKQAAgVAPgPQAPgPAWAAIDcAAQAVAAAPAPQAPAPAAAVIAAAKg");
	this.shape.setTransform(10.8,20.1,0.667,0.667);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRApIAAghQAAgQAKgOQAJgNAQgFIAAAIQgNAGgIAMQgJANABAPIAAAbg");
	this.shape_1.setTransform(8.1,8.8);

	this.instance = new lib.Symbol43();
	this.instance.parent = this;
	this.instance.setTransform(10.9,10.8,1,1,0,0,0,5.2,7.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfBJIAAg7QAAgeASgXQARgYAcgJIAAANQgZALgNAWQgPAXAAAbIAAAxg");
	this.shape_2.setTransform(42.8,6.5,0.667,0.667);

	this.instance_1 = new lib.Symbol44();
	this.instance_1.parent = this;
	this.instance_1.setTransform(46.1,8.7,1,1,0,0,0,6,8.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C15752").s().p("Ai+AkIAAgMQAAgYASgSQASgRAZgBIEDAAQAZABARARQATASgBAYIAAAMg");
	this.shape_3.setTransform(46.1,19.8,0.667,0.667);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#40516C").ss(3).p("ABvBPIgjAAIABgKIAAiEQAAgggWgXQgXgWggAAQgfAAgWAWQgXAXAAAgIABCOIgiAAQgWAAgPAPQgPAPAAAWIAAAKIFDAAIAAgKQAAgWgPgPQgPgPgVAAg");
	this.shape_4.setTransform(10.8,12.8,0.667,0.667);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#40516C").ss(3).p("ACCBdIgoAAIAAioQAAglgagbQgbgaglAAQglAAgaAaQgbAbAAAlIABCoIgoAAQgZAAgSASQgSASAAAZIAAAMIF8AAIAAgMQAAgZgSgSQgRgSgZAAg");
	this.shape_5.setTransform(46.1,11.1,0.667,0.667);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.instance_1},{t:this.shape_2},{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol42, new cjs.Rectangle(-1.5,-6.5,61.8,30.2), null);


(lib.Symbol39 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAUQgIgIAAgMQAAgLAIgIQAIgIALAAQAMAAAIAIQAIAIAAALQAAAMgIAIQgIAIgMAAQgLAAgIgIg");
	this.shape.setTransform(36.8,37.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1799));

	// Layer_4
	this.instance = new lib.Symbol41();
	this.instance.parent = this;
	this.instance.setTransform(36.9,37.1,1,1,-14.7,0,0,-1,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:359.5,y:37},1798).wait(1));

	// Layer_2
	this.instance_1 = new lib.Symbol40();
	this.instance_1.parent = this;
	this.instance_1.setTransform(36.8,37,1,1,0,0,0,2.8,18.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:2.9,rotation:4674.2,x:36.9},1798).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFA643").s().p("AjQDRQhWhXAAh6QAAh6BWhWQBXhWB5AAQB6AABXBWQBWBWAAB6QAAB6hWBXQhXBWh6AAQh5AAhXhWgAgNDZIAAAsQAAAEAFAAIARAAQAFAAAAgEIAAgsQAAgEgFAAIgRAAQgFAAAAAEgABzC0IgRAJQgEAEACADIAWAlQAEADACgCIARgJQACgBAAgFIgWglQgCgDgCAAIgCABgAh4C2IgWAlQgBADADADIARAJQACACAEgDIAWglQAAgFgCgCIgRgJIgCAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABgAjDBgIglAWQgDAFACABIAJARQAEAEACgCIAlgWQAEgEgCgCIgJgRQgDgDgCAAIgCABgAC9BiIgJARQgBADADADIAlAWQADABADgDIAJgRQAAgEgBgCIglgWIgDAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBABgADVgIIAAARQAAAFAEAAIAsAAQACAAACgFIAAgRQAAgFgEAAIgsAAQgEAAAAAFgAkHgIIAAARQAAAFADAAIAsAAQAEAAAAgFIAAgRQAAgFgEAAIgqAAQgFAAAAAFgADbiOIglAWQgEAEACACIAJARQAEAEADgCIAlgWQAEgEgDgCIgJgRQgCgDgCAAIgCABgAjgiMIgJARQgCACADAEIAlAWQAEABADgDIAJgRQACgCgEgEIglgWIgCAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABgAh7jpIgRAJQgEAEACACIAWAlQAEAEACgCIARgJQACgCAAgFIgWglIgFgCIgBABgAB2joIgWAlQgBAEADADIARAJQADABADgDIAWglQAAgFgCgBIgRgJIgCgBQgCAAgCACgAgNkEIAAAsQAAAEAFAAIARAAQAFAAAAgEIAAgsQAAgEgFAAIgRAAQgFAAAAAEg");
	this.shape_1.setTransform(37,37);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1799));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#34506D").s().p("AkFEGQhshsAAiaQAAiYBshtQBshsCZAAQCaAABsBsQBsBtAACYQAACahsBsQhsBsiaAAQiZAAhshsgAjkjkQhfBfAACFQAACGBfBfQBfBfCFgBQCGABBfhfQBfhfAAiGQAAiFhfhfQhfhfiGAAQiFAAhfBfg");
	this.shape_2.setTransform(37,37);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1799));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,74,74);


(lib.Symbol37 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol35();
	this.instance.parent = this;
	this.instance.setTransform(23.8,22.6,1,1,0,0,0,22.2,21.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol37, new cjs.Rectangle(0,0,57.7,44.7), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9AA0B5").s().p("Ag2AeIAAg7IBKAAQAPAAAKAKQAKAKAAAOIAAAZg");
	this.shape.setTransform(52,4.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#40516C").ss(3).p("ACWAtIAAgoQAAgUgOgPQgPgOgVAAIjHAAQgVAAgPAOQgOAPAAAUIAAAog");
	this.shape_1.setTransform(44,4.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B4B8C7").s().p("AiVAtIAAgoQAAgTAPgPQAPgPAUAAIDHAAQAVAAAPAPQAOAPAAATIAAAog");
	this.shape_2.setTransform(44,4.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[]},1).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},3).to({state:[]},117).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},15).to({state:[]},76).wait(38));

	// Layer_8
	this.instance = new lib.Symbol36();
	this.instance.parent = this;
	this.instance.setTransform(67.1,26.7,1,1,0,0,0,22.6,22.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(3).to({_off:false},0).wait(14).to({x:116.6},43,cjs.Ease.quadInOut).wait(30).to({x:112.1},20,cjs.Ease.quadInOut).to({_off:true},10).wait(15).to({_off:false},0).to({x:117.1},23,cjs.Ease.quadInOut).wait(18).to({x:67.1},33,cjs.Ease.quadInOut).to({_off:true},2).wait(38));

	// Layer_7
	this.instance_1 = new lib.Symbol37();
	this.instance_1.parent = this;
	this.instance_1.setTransform(22,26.7,1,1,0,0,0,23.1,22.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},1).wait(3).to({_off:false},0).wait(14).to({x:-30},43,cjs.Ease.quadInOut).wait(30).to({x:-24.5},20,cjs.Ease.quadInOut).to({_off:true},10).wait(15).to({_off:false},0).to({x:-30.5,y:26.6},23,cjs.Ease.quadInOut).wait(18).to({x:22,y:26.7},33,cjs.Ease.quadInOut).to({_off:true},2).wait(38));

	// Layer_9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("ACbC0IAAlsIBQAFIAAFsg");
	var mask_graphics_4 = new cjs.Graphics().p("ACbC0IAAlsIBQAFIAAFsg");
	var mask_graphics_18 = new cjs.Graphics().p("ACbC0IAAlsIBQAFIAAFsg");
	var mask_graphics_19 = new cjs.Graphics().p("ACJC0IAAlsIBoAFIAAFsg");
	var mask_graphics_20 = new cjs.Graphics().p("AB4C0IAAlsIB/AFIAAFsg");
	var mask_graphics_21 = new cjs.Graphics().p("ABnC0IAAlsICWAFIAAFsg");
	var mask_graphics_22 = new cjs.Graphics().p("ABVC0IAAlsICuAFIgBFsg");
	var mask_graphics_23 = new cjs.Graphics().p("ABEC0IAAlsIDEAFIAAFsg");
	var mask_graphics_24 = new cjs.Graphics().p("AAzC0IAAlsIDbAFIAAFsg");
	var mask_graphics_25 = new cjs.Graphics().p("AAhC0IABlsIDyAFIAAFsg");
	var mask_graphics_26 = new cjs.Graphics().p("AAQC0IAAlsIEKAFIgBFsg");
	var mask_graphics_27 = new cjs.Graphics().p("AAAC0IAAlsIEfAFIAAFsg");
	var mask_graphics_28 = new cjs.Graphics().p("AgSC0IABlsIE2AFIAAFsg");
	var mask_graphics_29 = new cjs.Graphics().p("AgjC0IABlsIFNAFIgBFsg");
	var mask_graphics_30 = new cjs.Graphics().p("Ag0C0IAAlsIFlAFIgBFsg");
	var mask_graphics_31 = new cjs.Graphics().p("AhFC0IAAlsIF7AFIAAFsg");
	var mask_graphics_32 = new cjs.Graphics().p("AhXC0IABlsIGSAFIAAFsg");
	var mask_graphics_33 = new cjs.Graphics().p("AhoC0IAAlsIGqAFIgBFsg");
	var mask_graphics_34 = new cjs.Graphics().p("Ah5C0IAAlsIHBAFIgBFsg");
	var mask_graphics_35 = new cjs.Graphics().p("AiLC0IABlsIHXAFIAAFsg");
	var mask_graphics_36 = new cjs.Graphics().p("AicC0IABlsIHuAFIAAFsg");
	var mask_graphics_37 = new cjs.Graphics().p("AitC0IAAlsIIGAFIgBFsg");
	var mask_graphics_38 = new cjs.Graphics().p("Ai/C0IABlsIIdAFIgBFsg");
	var mask_graphics_39 = new cjs.Graphics().p("AjQC0IABlsIIzAFIAAFsg");
	var mask_graphics_40 = new cjs.Graphics().p("AjhC0IAAlsIJLAFIAAFsg");
	var mask_graphics_41 = new cjs.Graphics().p("AjyC0IAAlsIJiAFIgBFsg");
	var mask_graphics_42 = new cjs.Graphics().p("AkEC0IABlsIJ5AFIgBFsg");
	var mask_graphics_43 = new cjs.Graphics().p("AkVC0IABlsIKPAFIAAFsg");
	var mask_graphics_44 = new cjs.Graphics().p("AkmC0IAAlsIKnAFIgBFsg");
	var mask_graphics_45 = new cjs.Graphics().p("Ak4C0IABlsIK+AFIgBFsg");
	var mask_graphics_46 = new cjs.Graphics().p("AlJC0IABlsILVAFIgBFsg");
	var mask_graphics_47 = new cjs.Graphics().p("AlaC0IABlsILrAFIAAFsg");
	var mask_graphics_48 = new cjs.Graphics().p("AlsC0IABlsIMDAFIgBFsg");
	var mask_graphics_49 = new cjs.Graphics().p("Al9C0IABlsIMaAFIgBFsg");
	var mask_graphics_50 = new cjs.Graphics().p("AmOC0IABlsIMxAFIgBFsg");
	var mask_graphics_51 = new cjs.Graphics().p("AmfC0IAAlsINJAFIgBFsg");
	var mask_graphics_52 = new cjs.Graphics().p("AmwC0IABlsINgAFIgBFsg");
	var mask_graphics_53 = new cjs.Graphics().p("Am7C0IABlsIN2AFIgBFsg");
	var mask_graphics_54 = new cjs.Graphics().p("AnHC0IABlsIOOAFIgBFsg");
	var mask_graphics_55 = new cjs.Graphics().p("AnSC0IABlsIOkAFIgBFsg");
	var mask_graphics_56 = new cjs.Graphics().p("AneC0IABlsIO8AFIgBFsg");
	var mask_graphics_57 = new cjs.Graphics().p("AnpC0IABlsIPSAFIgBFsg");
	var mask_graphics_58 = new cjs.Graphics().p("An1C0IABlsIPqAFIgBFsg");
	var mask_graphics_59 = new cjs.Graphics().p("AoAC0IABlsIQAAFIgBFsg");
	var mask_graphics_60 = new cjs.Graphics().p("AoMC0IABlsIQYAFIgBFsg");
	var mask_graphics_61 = new cjs.Graphics().p("AoXC0IABlsIQuAFIgBFsg");
	var mask_graphics_136 = new cjs.Graphics().p("AoXC0IABlsIQuAFIgBFsg");
	var mask_graphics_177 = new cjs.Graphics().p("ApJC0IABlsISSAFIgBFsg");
	var mask_graphics_178 = new cjs.Graphics().p("Ao5C0IAClsIRxAFIgCFsg");
	var mask_graphics_179 = new cjs.Graphics().p("AooC0IABlsIRQAFIgBFsg");
	var mask_graphics_180 = new cjs.Graphics().p("AoYC0IAClsIQvAFIgCFsg");
	var mask_graphics_181 = new cjs.Graphics().p("AoHC0IABlsIQOAFIgBFsg");
	var mask_graphics_182 = new cjs.Graphics().p("An2C0IABlsIPsAFIgBFsg");
	var mask_graphics_183 = new cjs.Graphics().p("AnmC0IABlsIPMAFIgBFsg");
	var mask_graphics_184 = new cjs.Graphics().p("AnVC0IABlsIOqAFIgBFsg");
	var mask_graphics_185 = new cjs.Graphics().p("AnFC0IABlsIOKAFIgBFsg");
	var mask_graphics_186 = new cjs.Graphics().p("Am0C0IABlsINoAFIgBFsg");
	var mask_graphics_187 = new cjs.Graphics().p("AmfC0IABlsINIAFIgBFsg");
	var mask_graphics_188 = new cjs.Graphics().p("AmGC0IABlsIMnAFIgBFsg");
	var mask_graphics_189 = new cjs.Graphics().p("AltC0IABlsIMFAFIAAFsg");
	var mask_graphics_190 = new cjs.Graphics().p("AlUC0IAAlsILlAFIgBFsg");
	var mask_graphics_191 = new cjs.Graphics().p("Ak8C0IABlsILEAFIgBFsg");
	var mask_graphics_192 = new cjs.Graphics().p("AkjC0IABlsIKjAFIgBFsg");
	var mask_graphics_193 = new cjs.Graphics().p("AkKC0IABlsIKBAFIAAFsg");
	var mask_graphics_194 = new cjs.Graphics().p("AjxC0IAAlsIJhAFIgBFsg");
	var mask_graphics_195 = new cjs.Graphics().p("AjYC0IAAlsIJAAFIgBFsg");
	var mask_graphics_196 = new cjs.Graphics().p("AjAC0IABlsIIeAFIAAFsg");
	var mask_graphics_197 = new cjs.Graphics().p("AinC0IABlsIH9AFIAAFsg");
	var mask_graphics_198 = new cjs.Graphics().p("AiOC0IABlsIHcAFIgBFsg");
	var mask_graphics_199 = new cjs.Graphics().p("Ah1C0IAAlsIG8AFIgBFsg");
	var mask_graphics_200 = new cjs.Graphics().p("AhcC0IAAlsIGaAFIAAFsg");
	var mask_graphics_201 = new cjs.Graphics().p("AhEC0IABlsIF5AFIAAFsg");
	var mask_graphics_202 = new cjs.Graphics().p("AgrC0IABlsIFYAFIgBFsg");
	var mask_graphics_203 = new cjs.Graphics().p("AgSC0IAAlsIE3AFIAAFsg");
	var mask_graphics_204 = new cjs.Graphics().p("AAGC0IAAlsIEXAFIAAFsg");
	var mask_graphics_205 = new cjs.Graphics().p("AAfC0IAAlsID2AFIAAFsg");
	var mask_graphics_206 = new cjs.Graphics().p("AA3C0IABlsIDVAFIgBFsg");
	var mask_graphics_207 = new cjs.Graphics().p("ABQC0IAAlsIC0AFIAAFsg");
	var mask_graphics_208 = new cjs.Graphics().p("ABpC0IAAlsICTAFIAAFsg");
	var mask_graphics_209 = new cjs.Graphics().p("ACCC0IAAlsIByAFIAAFsg");
	var mask_graphics_210 = new cjs.Graphics().p("ACbC0IAAlsIBQAFIAAFsg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:23.5,y:14.6}).wait(1).to({graphics:null,x:0,y:0}).wait(3).to({graphics:mask_graphics_4,x:23.5,y:14.6}).wait(14).to({graphics:mask_graphics_18,x:23.5,y:14.6}).wait(1).to({graphics:mask_graphics_19,x:24.1,y:14.6}).wait(1).to({graphics:mask_graphics_20,x:24.7,y:14.6}).wait(1).to({graphics:mask_graphics_21,x:25.3,y:14.6}).wait(1).to({graphics:mask_graphics_22,x:25.9,y:14.6}).wait(1).to({graphics:mask_graphics_23,x:26.4,y:14.6}).wait(1).to({graphics:mask_graphics_24,x:27,y:14.6}).wait(1).to({graphics:mask_graphics_25,x:27.6,y:14.6}).wait(1).to({graphics:mask_graphics_26,x:28.2,y:14.6}).wait(1).to({graphics:mask_graphics_27,x:28.7,y:14.6}).wait(1).to({graphics:mask_graphics_28,x:29.3,y:14.6}).wait(1).to({graphics:mask_graphics_29,x:29.9,y:14.6}).wait(1).to({graphics:mask_graphics_30,x:30.5,y:14.6}).wait(1).to({graphics:mask_graphics_31,x:31,y:14.6}).wait(1).to({graphics:mask_graphics_32,x:31.6,y:14.6}).wait(1).to({graphics:mask_graphics_33,x:32.2,y:14.6}).wait(1).to({graphics:mask_graphics_34,x:32.8,y:14.6}).wait(1).to({graphics:mask_graphics_35,x:33.3,y:14.6}).wait(1).to({graphics:mask_graphics_36,x:33.9,y:14.6}).wait(1).to({graphics:mask_graphics_37,x:34.5,y:14.6}).wait(1).to({graphics:mask_graphics_38,x:35.1,y:14.6}).wait(1).to({graphics:mask_graphics_39,x:35.6,y:14.6}).wait(1).to({graphics:mask_graphics_40,x:36.2,y:14.6}).wait(1).to({graphics:mask_graphics_41,x:36.8,y:14.6}).wait(1).to({graphics:mask_graphics_42,x:37.4,y:14.6}).wait(1).to({graphics:mask_graphics_43,x:37.9,y:14.6}).wait(1).to({graphics:mask_graphics_44,x:38.5,y:14.6}).wait(1).to({graphics:mask_graphics_45,x:39.1,y:14.6}).wait(1).to({graphics:mask_graphics_46,x:39.7,y:14.6}).wait(1).to({graphics:mask_graphics_47,x:40.2,y:14.6}).wait(1).to({graphics:mask_graphics_48,x:40.8,y:14.6}).wait(1).to({graphics:mask_graphics_49,x:41.4,y:14.6}).wait(1).to({graphics:mask_graphics_50,x:42,y:14.6}).wait(1).to({graphics:mask_graphics_51,x:42.6,y:14.6}).wait(1).to({graphics:mask_graphics_52,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_53,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_54,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_55,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_56,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_57,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_58,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_59,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_60,x:43,y:14.6}).wait(1).to({graphics:mask_graphics_61,x:43.2,y:14.6}).wait(60).to({graphics:null,x:0,y:0}).wait(15).to({graphics:mask_graphics_136,x:43.2,y:14.6}).wait(41).to({graphics:mask_graphics_177,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_178,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_179,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_180,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_181,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_182,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_183,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_184,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_185,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_186,x:43.1,y:14.6}).wait(1).to({graphics:mask_graphics_187,x:42.6,y:14.6}).wait(1).to({graphics:mask_graphics_188,x:41.8,y:14.6}).wait(1).to({graphics:mask_graphics_189,x:40.9,y:14.6}).wait(1).to({graphics:mask_graphics_190,x:40.1,y:14.6}).wait(1).to({graphics:mask_graphics_191,x:39.3,y:14.6}).wait(1).to({graphics:mask_graphics_192,x:38.5,y:14.6}).wait(1).to({graphics:mask_graphics_193,x:37.6,y:14.6}).wait(1).to({graphics:mask_graphics_194,x:36.8,y:14.6}).wait(1).to({graphics:mask_graphics_195,x:36,y:14.6}).wait(1).to({graphics:mask_graphics_196,x:35.1,y:14.6}).wait(1).to({graphics:mask_graphics_197,x:34.3,y:14.6}).wait(1).to({graphics:mask_graphics_198,x:33.5,y:14.6}).wait(1).to({graphics:mask_graphics_199,x:32.7,y:14.6}).wait(1).to({graphics:mask_graphics_200,x:31.8,y:14.6}).wait(1).to({graphics:mask_graphics_201,x:31,y:14.6}).wait(1).to({graphics:mask_graphics_202,x:30.2,y:14.6}).wait(1).to({graphics:mask_graphics_203,x:29.3,y:14.6}).wait(1).to({graphics:mask_graphics_204,x:28.5,y:14.6}).wait(1).to({graphics:mask_graphics_205,x:27.7,y:14.6}).wait(1).to({graphics:mask_graphics_206,x:26.9,y:14.6}).wait(1).to({graphics:mask_graphics_207,x:26,y:14.6}).wait(1).to({graphics:mask_graphics_208,x:25.2,y:14.6}).wait(1).to({graphics:mask_graphics_209,x:24.4,y:14.6}).wait(1).to({graphics:mask_graphics_210,x:23.5,y:14.6}).wait(2).to({graphics:null,x:0,y:0}).wait(38));

	// Layer_5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D5DDEF").s().p("AoPBJIADiRIQcAAIgFCRg");
	this.shape_3.setTransform(42.9,14.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9AA0B5").s().p("AoMAUIAAgnIQZAAIAAAng");
	this.shape_4.setTransform(42.7,24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#40516C").s().p("AoPB7IAAgeIQaAAIAAAegAoMhcIACgeIQaAAIAAAeIwcAAg");
	this.shape_5.setTransform(42.9,16.7);

	var maskedShapeInstanceList = [this.shape_3,this.shape_4,this.shape_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).to({state:[]},1).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]},3).to({state:[]},117).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]},15).to({state:[]},76).wait(38));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.1,-1.5,90.6,50.5);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(44.3,17.8,1,1,0,0,0,18.1,17.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(40).to({regY:17.9,rotation:-5.7,x:37.8,y:37.4},43,cjs.Ease.quadInOut).to({x:-61.3,y:4.9},45,cjs.Ease.quadInOut).wait(203).to({x:57.7,y:38.4},43,cjs.Ease.quadInOut).wait(105).to({regY:17.8,rotation:0,x:44.3,y:17.8},53,cjs.Ease.quadInOut).wait(35));

	// Layer_2
	this.instance_1 = new lib.Symbol27("single",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(44,120,1,1,0,0,0,44,24);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(40).to({startPosition:0},0).to({regY:24.1,rotation:-0.2,x:37.9,y:139.7},43,cjs.Ease.quadInOut).to({x:-61.1,y:107.2},45,cjs.Ease.quadInOut).wait(5).to({mode:"synched",startPosition:4,loop:false},0).to({y:212.9,startPosition:72},68,cjs.Ease.quadInOut).wait(49).to({y:212.2,mode:"single",startPosition:111},0).to({y:112.2},38,cjs.Ease.quadInOut).wait(43).to({startPosition:111},0).to({x:55.9,y:138.7,startPosition:112},43,cjs.Ease.quadInOut).wait(29).to({mode:"synched",startPosition:136,loop:false},0).wait(76).to({mode:"single",startPosition:0},0).to({regY:24,rotation:0,x:44,y:120},53,cjs.Ease.quadInOut).wait(35));

	// Layer_11
	this.instance_2 = new lib.Symbol32();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-61.4,177.1,1,1,-0.4,0,0,70.9,43.3);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(531).to({_off:false},0).to({y:263.1,alpha:1},35,cjs.Ease.bounceOut).wait(1));

	// Layer_10
	this.instance_3 = new lib.Symbol32();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-61.4,263.1,1,1,-0.4,0,0,70.9,43.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(250).to({y:163.1},38,cjs.Ease.quadInOut).wait(43).to({x:55.6,y:190.1},43,cjs.Ease.quadInOut).wait(33).to({y:212.1},25,cjs.Ease.bounceOut).to({x:255.6},100).wait(35));

	// Layer_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("Ah1H8IAArtIHLAAIAALtg");
	var mask_graphics_40 = new cjs.Graphics().p("Ah1H8IAArtIHLAAIAALtg");
	var mask_graphics_41 = new cjs.Graphics().p("Ah2H+IAArtIHLAAIAALtg");
	var mask_graphics_42 = new cjs.Graphics().p("Ah3IAIAArtIHLAAIAALtg");
	var mask_graphics_43 = new cjs.Graphics().p("Ah3ICIAArtIHLAAIAALtg");
	var mask_graphics_44 = new cjs.Graphics().p("Ah4IFIAArtIHLAAIAALtg");
	var mask_graphics_45 = new cjs.Graphics().p("Ah5IHIAArtIHLAAIAALtg");
	var mask_graphics_46 = new cjs.Graphics().p("Ah5IJIAArtIHLAAIAALtg");
	var mask_graphics_47 = new cjs.Graphics().p("Ah6ILIAArtIHLAAIAALtg");
	var mask_graphics_48 = new cjs.Graphics().p("Ah7INIAArtIHLAAIAALtg");
	var mask_graphics_49 = new cjs.Graphics().p("Ah8IQIAArtIHLAAIAALtg");
	var mask_graphics_50 = new cjs.Graphics().p("Ah8ISIAArtIHLAAIAALtg");
	var mask_graphics_51 = new cjs.Graphics().p("Ah9IUIAArtIHLAAIAALtg");
	var mask_graphics_52 = new cjs.Graphics().p("Ah+IWIAArtIHLAAIAALtg");
	var mask_graphics_53 = new cjs.Graphics().p("Ah+IYIAArtIHLAAIAALtg");
	var mask_graphics_54 = new cjs.Graphics().p("Ah/IbIAArtIHLAAIAALtg");
	var mask_graphics_55 = new cjs.Graphics().p("AiAIdIAArtIHLAAIAALtg");
	var mask_graphics_56 = new cjs.Graphics().p("AiAIfIAArtIHLAAIAALtg");
	var mask_graphics_57 = new cjs.Graphics().p("AiBIhIAArtIHLAAIAALtg");
	var mask_graphics_58 = new cjs.Graphics().p("AiCIkIAArtIHLAAIAALtg");
	var mask_graphics_59 = new cjs.Graphics().p("AiDImIAArtIHLAAIAALtg");
	var mask_graphics_60 = new cjs.Graphics().p("AiDIoIAArtIHLAAIAALtg");
	var mask_graphics_61 = new cjs.Graphics().p("AiEIqIAArtIHLAAIAALtg");
	var mask_graphics_62 = new cjs.Graphics().p("AiFIsIAArtIHLAAIAALtg");
	var mask_graphics_63 = new cjs.Graphics().p("AiFIvIAArtIHLAAIAALtg");
	var mask_graphics_64 = new cjs.Graphics().p("AiGIxIAArtIHLAAIAALtg");
	var mask_graphics_65 = new cjs.Graphics().p("AiHIzIAArtIHLAAIAALtg");
	var mask_graphics_66 = new cjs.Graphics().p("AiHI1IAArtIHLAAIAALtg");
	var mask_graphics_67 = new cjs.Graphics().p("AiII3IAArtIHLAAIAALtg");
	var mask_graphics_68 = new cjs.Graphics().p("AiJI6IAArtIHLAAIAALtg");
	var mask_graphics_69 = new cjs.Graphics().p("AiJI8IAArtIHLAAIAALtg");
	var mask_graphics_70 = new cjs.Graphics().p("AiKI+IAArtIHLAAIAALtg");
	var mask_graphics_71 = new cjs.Graphics().p("AiLJAIAArtIHLAAIAALtg");
	var mask_graphics_72 = new cjs.Graphics().p("AiMJCIAArtIHLAAIAALtg");
	var mask_graphics_73 = new cjs.Graphics().p("AiMJFIAArtIHLAAIAALtg");
	var mask_graphics_74 = new cjs.Graphics().p("AiNJHIAArtIHLAAIAALtg");
	var mask_graphics_75 = new cjs.Graphics().p("AiOJJIAArtIHLAAIAALtg");
	var mask_graphics_76 = new cjs.Graphics().p("AiOJLIAArtIHLAAIAALtg");
	var mask_graphics_77 = new cjs.Graphics().p("AiPJNIAArtIHLAAIAALtg");
	var mask_graphics_78 = new cjs.Graphics().p("AiQJQIAArtIHLAAIAALtg");
	var mask_graphics_79 = new cjs.Graphics().p("AiQJSIAArtIHLAAIAALtg");
	var mask_graphics_80 = new cjs.Graphics().p("AiRJUIAArtIHLAAIAALtg");
	var mask_graphics_81 = new cjs.Graphics().p("AiSJWIAArtIHLAAIAALtg");
	var mask_graphics_82 = new cjs.Graphics().p("AiTJZIAArtIHLAAIAALtg");
	var mask_graphics_83 = new cjs.Graphics().p("AiTJbIAArtIHLAAIAALtg");
	var mask_graphics_84 = new cjs.Graphics().p("AieJXIAArtIHLAAIAALtg");
	var mask_graphics_85 = new cjs.Graphics().p("AipJUIAArtIHLAAIAALtg");
	var mask_graphics_86 = new cjs.Graphics().p("Ai1JQIAArtIHLAAIAALtg");
	var mask_graphics_87 = new cjs.Graphics().p("AjAJNIAArtIHLAAIAALtg");
	var mask_graphics_88 = new cjs.Graphics().p("AjLJKIAArtIHLAAIAALtg");
	var mask_graphics_89 = new cjs.Graphics().p("AjWJGIAArtIHLAAIAALtg");
	var mask_graphics_90 = new cjs.Graphics().p("AjhJDIAArtIHLAAIAALtg");
	var mask_graphics_91 = new cjs.Graphics().p("AjlI/IAArtIHLAAIAALtg");
	var mask_graphics_92 = new cjs.Graphics().p("AjlI8IAArtIHLAAIAALtg");
	var mask_graphics_93 = new cjs.Graphics().p("AjlI4IAArtIHLAAIAALtg");
	var mask_graphics_94 = new cjs.Graphics().p("AjlI1IAArtIHLAAIAALtg");
	var mask_graphics_95 = new cjs.Graphics().p("AjlIxIAArtIHLAAIAALtg");
	var mask_graphics_96 = new cjs.Graphics().p("AjlIuIAArtIHLAAIAALtg");
	var mask_graphics_97 = new cjs.Graphics().p("AjlIrIAArtIHLAAIAALtg");
	var mask_graphics_98 = new cjs.Graphics().p("AjlInIAArtIHLAAIAALtg");
	var mask_graphics_99 = new cjs.Graphics().p("AjlIkIAArtIHLAAIAALtg");
	var mask_graphics_100 = new cjs.Graphics().p("AjlIgIAArtIHLAAIAALtg");
	var mask_graphics_101 = new cjs.Graphics().p("AjlIdIAArtIHLAAIAALtg");
	var mask_graphics_102 = new cjs.Graphics().p("AjlIZIAArtIHLAAIAALtg");
	var mask_graphics_103 = new cjs.Graphics().p("AjlIWIAArtIHLAAIAALtg");
	var mask_graphics_104 = new cjs.Graphics().p("AjlISIAArtIHLAAIAALtg");
	var mask_graphics_105 = new cjs.Graphics().p("AjlIPIAArtIHLAAIAALtg");
	var mask_graphics_106 = new cjs.Graphics().p("AjlIMIAArtIHLAAIAALtg");
	var mask_graphics_107 = new cjs.Graphics().p("AjlIIIAArtIHLAAIAALtg");
	var mask_graphics_108 = new cjs.Graphics().p("AjlIFIAArtIHLAAIAALtg");
	var mask_graphics_109 = new cjs.Graphics().p("AjlIBIAArtIHLAAIAALtg");
	var mask_graphics_110 = new cjs.Graphics().p("AjlH+IAArtIHLAAIAALtg");
	var mask_graphics_111 = new cjs.Graphics().p("AjlH6IAArtIHLAAIAALtg");
	var mask_graphics_112 = new cjs.Graphics().p("AjvH3IAArtIHLAAIAALtg");
	var mask_graphics_113 = new cjs.Graphics().p("Aj7HzIAArtIHLAAIAALtg");
	var mask_graphics_114 = new cjs.Graphics().p("AkGHwIAArtIHLAAIAALtg");
	var mask_graphics_115 = new cjs.Graphics().p("AkRHtIAArtIHLAAIAALtg");
	var mask_graphics_116 = new cjs.Graphics().p("AkcHpIAArtIHLAAIAALtg");
	var mask_graphics_117 = new cjs.Graphics().p("AknHmIAArtIHLAAIAALtg");
	var mask_graphics_118 = new cjs.Graphics().p("AkyHiIAArtIHLAAIAALtg");
	var mask_graphics_119 = new cjs.Graphics().p("Ak9HfIAArtIHLAAIAALtg");
	var mask_graphics_120 = new cjs.Graphics().p("AlIHbIAArtIHLAAIAALtg");
	var mask_graphics_121 = new cjs.Graphics().p("AlTHYIAArtIHLAAIAALtg");
	var mask_graphics_122 = new cjs.Graphics().p("AlfHUIAArtIHLAAIAALtg");
	var mask_graphics_123 = new cjs.Graphics().p("AlqHRIAArtIHLAAIAALtg");
	var mask_graphics_124 = new cjs.Graphics().p("Al1HOIAArtIHLAAIAALtg");
	var mask_graphics_125 = new cjs.Graphics().p("AmAHKIAArtIHLAAIAALtg");
	var mask_graphics_126 = new cjs.Graphics().p("AmLHHIAArtIHLAAIAALtg");
	var mask_graphics_127 = new cjs.Graphics().p("AmWHDIAArtIHLAAIAALtg");
	var mask_graphics_128 = new cjs.Graphics().p("AmhHAIAArtIHLAAIAALtg");
	var mask_graphics_133 = new cjs.Graphics().p("AmhHAIAArtIHLAAIAALtg");
	var mask_graphics_134 = new cjs.Graphics().p("AmhHIIAAr9IHLAAIAAL9g");
	var mask_graphics_135 = new cjs.Graphics().p("AmhHQIAAsNIHLAAIAAMNg");
	var mask_graphics_136 = new cjs.Graphics().p("AmhHYIAAsdIHLAAIAAMdg");
	var mask_graphics_137 = new cjs.Graphics().p("AmhHgIAAsuIHLAAIAAMug");
	var mask_graphics_138 = new cjs.Graphics().p("AmhHoIAAs+IHLAAIAAM+g");
	var mask_graphics_139 = new cjs.Graphics().p("AmhHwIAAtOIHLAAIAANOg");
	var mask_graphics_140 = new cjs.Graphics().p("AmhH5IAAtfIHLAAIAANfg");
	var mask_graphics_141 = new cjs.Graphics().p("AmhIBIAAtvIHLAAIAANvg");
	var mask_graphics_142 = new cjs.Graphics().p("AmhIJIAAt/IHLAAIAAN/g");
	var mask_graphics_143 = new cjs.Graphics().p("AmhIRIAAuPIHLAAIAAOPg");
	var mask_graphics_144 = new cjs.Graphics().p("AmhIZIAAufIHLAAIAAOfg");
	var mask_graphics_145 = new cjs.Graphics().p("AmhIhIAAuvIHLAAIAAOvg");
	var mask_graphics_146 = new cjs.Graphics().p("AmhIpIAAu/IHLAAIAAO/g");
	var mask_graphics_147 = new cjs.Graphics().p("AmhIxIAAvPIHLAAIAAPPg");
	var mask_graphics_148 = new cjs.Graphics().p("AmhI6IAAvgIHLAAIAAPgg");
	var mask_graphics_149 = new cjs.Graphics().p("AmhJCIAAvwIHLAAIAAPwg");
	var mask_graphics_150 = new cjs.Graphics().p("AmhJKIAAwAIHLAAIAAQAg");
	var mask_graphics_151 = new cjs.Graphics().p("AmhJSIAAwQIHLAAIAAQQg");
	var mask_graphics_152 = new cjs.Graphics().p("AmhJaIAAwgIHLAAIAAQgg");
	var mask_graphics_153 = new cjs.Graphics().p("AmhJiIAAwxIHLAAIAAQxg");
	var mask_graphics_154 = new cjs.Graphics().p("AmhJqIAAxBIHLAAIAARBg");
	var mask_graphics_155 = new cjs.Graphics().p("AmhJyIAAxRIHLAAIAARRg");
	var mask_graphics_156 = new cjs.Graphics().p("AmhJ6IAAxhIHLAAIAARhg");
	var mask_graphics_157 = new cjs.Graphics().p("AmhKDIAAxyIHLAAIAARyg");
	var mask_graphics_158 = new cjs.Graphics().p("AmhKLIAAyCIHLAAIAASCg");
	var mask_graphics_159 = new cjs.Graphics().p("AmhKTIAAySIHLAAIAASSg");
	var mask_graphics_160 = new cjs.Graphics().p("AmhKbIAAyiIHLAAIAASig");
	var mask_graphics_161 = new cjs.Graphics().p("AmhKjIAAyyIHLAAIAASyg");
	var mask_graphics_162 = new cjs.Graphics().p("AmhKrIAAzCIHLAAIAATCg");
	var mask_graphics_163 = new cjs.Graphics().p("AmhKzIAAzSIHLAAIAATSg");
	var mask_graphics_164 = new cjs.Graphics().p("AmhK7IAAziIHLAAIAATig");
	var mask_graphics_165 = new cjs.Graphics().p("AmhLEIAAzzIHLAAIAATzg");
	var mask_graphics_166 = new cjs.Graphics().p("AmhLMIAA0DIHLAAIAAUDg");
	var mask_graphics_167 = new cjs.Graphics().p("AmhLUIAA0TIHLAAIAAUTg");
	var mask_graphics_168 = new cjs.Graphics().p("AmhLcIAA0kIHLAAIAAUkg");
	var mask_graphics_169 = new cjs.Graphics().p("AmhLkIAA00IHLAAIAAU0g");
	var mask_graphics_170 = new cjs.Graphics().p("AmhLsIAA1EIHLAAIAAVEg");
	var mask_graphics_171 = new cjs.Graphics().p("AmhL0IAA1UIHLAAIAAVUg");
	var mask_graphics_172 = new cjs.Graphics().p("AmhL8IAA1kIHLAAIAAVkg");
	var mask_graphics_173 = new cjs.Graphics().p("AmhMFIAA11IHLAAIAAV1g");
	var mask_graphics_174 = new cjs.Graphics().p("AmhMNIAA2FIHLAAIAAWFg");
	var mask_graphics_175 = new cjs.Graphics().p("AmhMVIAA2VIHLAAIAAWVg");
	var mask_graphics_176 = new cjs.Graphics().p("AmhMdIAA2lIHLAAIAAWlg");
	var mask_graphics_177 = new cjs.Graphics().p("AmhMlIAA21IHLAAIAAW1g");
	var mask_graphics_178 = new cjs.Graphics().p("AmhMtIAA3FIHLAAIAAXFg");
	var mask_graphics_179 = new cjs.Graphics().p("AmhM1IAA3VIHLAAIAAXVg");
	var mask_graphics_180 = new cjs.Graphics().p("AmhM9IAA3lIHLAAIAAXlg");
	var mask_graphics_181 = new cjs.Graphics().p("AmhNFIAA31IHLAAIAAX1g");
	var mask_graphics_182 = new cjs.Graphics().p("AmhNOIAA4GIHLAAIAAYGg");
	var mask_graphics_183 = new cjs.Graphics().p("AmhNWIAA4WIHLAAIAAYWg");
	var mask_graphics_184 = new cjs.Graphics().p("AmhNeIAA4nIHLAAIAAYng");
	var mask_graphics_185 = new cjs.Graphics().p("AmhNmIAA43IHLAAIAAY3g");
	var mask_graphics_186 = new cjs.Graphics().p("AmhNuIAA5HIHLAAIAAZHg");
	var mask_graphics_187 = new cjs.Graphics().p("AmhN2IAA5XIHLAAIAAZXg");
	var mask_graphics_188 = new cjs.Graphics().p("AmhN+IAA5nIHLAAIAAZng");
	var mask_graphics_189 = new cjs.Graphics().p("AmhOGIAA53IHLAAIAAZ3g");
	var mask_graphics_190 = new cjs.Graphics().p("AmhOPIAA6IIHLAAIAAaIg");
	var mask_graphics_191 = new cjs.Graphics().p("AmhOXIAA6YIHLAAIAAaYg");
	var mask_graphics_192 = new cjs.Graphics().p("AmhOfIAA6oIHLAAIAAaog");
	var mask_graphics_193 = new cjs.Graphics().p("AmhOnIAA64IHLAAIAAa4g");
	var mask_graphics_194 = new cjs.Graphics().p("AmhOvIAA7IIHLAAIAAbIg");
	var mask_graphics_195 = new cjs.Graphics().p("AmhO3IAA7YIHLAAIAAbYg");
	var mask_graphics_196 = new cjs.Graphics().p("AmhO/IAA7oIHLAAIAAbog");
	var mask_graphics_197 = new cjs.Graphics().p("AmhPHIAA74IHLAAIAAb4g");
	var mask_graphics_198 = new cjs.Graphics().p("AmhPPIAA8IIHLAAIAAcIg");
	var mask_graphics_199 = new cjs.Graphics().p("AmhPYIAA8aIHLAAIAAcag");
	var mask_graphics_200 = new cjs.Graphics().p("AmhPgIAA8qIHLAAIAAcqg");
	var mask_graphics_201 = new cjs.Graphics().p("AmhPmIAA85IHLAAIAAc5g");
	var mask_graphics_250 = new cjs.Graphics().p("AmhPmIAA85IHLAAIAAc5g");
	var mask_graphics_251 = new cjs.Graphics().p("AmhPZIAA8fIHLAAIAAcfg");
	var mask_graphics_252 = new cjs.Graphics().p("AmhPMIAA8FIHLAAIAAcFg");
	var mask_graphics_253 = new cjs.Graphics().p("AmhO/IAA7rIHLAAIAAbrg");
	var mask_graphics_254 = new cjs.Graphics().p("AmhOxIAA7QIHLAAIAAbQg");
	var mask_graphics_255 = new cjs.Graphics().p("AmhOkIAA62IHLAAIAAa2g");
	var mask_graphics_256 = new cjs.Graphics().p("AmhOXIAA6bIHLAAIAAabg");
	var mask_graphics_257 = new cjs.Graphics().p("AmhOKIAA6BIHLAAIAAaBg");
	var mask_graphics_258 = new cjs.Graphics().p("AmhN9IAA5nIHLAAIAAZng");
	var mask_graphics_259 = new cjs.Graphics().p("AmhNwIAA5NIHLAAIAAZNg");
	var mask_graphics_260 = new cjs.Graphics().p("AmhNiIAA4yIHLAAIAAYyg");
	var mask_graphics_261 = new cjs.Graphics().p("AmhNVIAA4YIHLAAIAAYYg");
	var mask_graphics_262 = new cjs.Graphics().p("AmhNIIAA39IHLAAIAAX9g");
	var mask_graphics_263 = new cjs.Graphics().p("AmhM7IAA3jIHLAAIAAXjg");
	var mask_graphics_264 = new cjs.Graphics().p("AmhMuIAA3JIHLAAIAAXJg");
	var mask_graphics_265 = new cjs.Graphics().p("AmhMhIAA2vIHLAAIAAWvg");
	var mask_graphics_266 = new cjs.Graphics().p("AmhMUIAA2VIHLAAIAAWVg");
	var mask_graphics_267 = new cjs.Graphics().p("AmhMGIAA15IHLAAIAAV5g");
	var mask_graphics_268 = new cjs.Graphics().p("AmhL5IAA1fIHLAAIAAVfg");
	var mask_graphics_269 = new cjs.Graphics().p("AmhLsIAA1FIHLAAIAAVFg");
	var mask_graphics_270 = new cjs.Graphics().p("AmhLfIAA0rIHLAAIAAUrg");
	var mask_graphics_271 = new cjs.Graphics().p("AmhLSIAA0RIHLAAIAAURg");
	var mask_graphics_272 = new cjs.Graphics().p("AmhLFIAAz3IHLAAIAAT3g");
	var mask_graphics_273 = new cjs.Graphics().p("AmhK4IAAzcIHLAAIAATcg");
	var mask_graphics_274 = new cjs.Graphics().p("AmhKqIAAzBIHLAAIAATBg");
	var mask_graphics_275 = new cjs.Graphics().p("AmhKdIAAynIHLAAIAASng");
	var mask_graphics_276 = new cjs.Graphics().p("AmhKQIAAyNIHLAAIAASNg");
	var mask_graphics_277 = new cjs.Graphics().p("AmhKDIAAxzIHLAAIAARzg");
	var mask_graphics_278 = new cjs.Graphics().p("AmhJ2IAAxZIHLAAIAARZg");
	var mask_graphics_279 = new cjs.Graphics().p("AmhJpIAAw+IHLAAIAAQ+g");
	var mask_graphics_280 = new cjs.Graphics().p("AmhJbIAAwjIHLAAIAAQjg");
	var mask_graphics_281 = new cjs.Graphics().p("AmhJOIAAwJIHLAAIAAQJg");
	var mask_graphics_282 = new cjs.Graphics().p("AmhJBIAAvvIHLAAIAAPvg");
	var mask_graphics_283 = new cjs.Graphics().p("AmhI0IAAvVIHLAAIAAPVg");
	var mask_graphics_284 = new cjs.Graphics().p("AmhInIAAu6IHLAAIAAO6g");
	var mask_graphics_285 = new cjs.Graphics().p("AmhIaIAAugIHLAAIAAOgg");
	var mask_graphics_286 = new cjs.Graphics().p("AmhINIAAuGIHLAAIAAOGg");
	var mask_graphics_287 = new cjs.Graphics().p("AmhH/IAAtrIHLAAIAANrg");
	var mask_graphics_288 = new cjs.Graphics().p("AmhHyIAAtRIHLAAIAANRg");
	var mask_graphics_331 = new cjs.Graphics().p("AmhHyIAAtRIHLAAIAANRg");
	var mask_graphics_332 = new cjs.Graphics().p("AmUH1IAAtRIHLAAIAANRg");
	var mask_graphics_333 = new cjs.Graphics().p("AmGH4IAAtRIHLAAIAANRg");
	var mask_graphics_334 = new cjs.Graphics().p("Al4H7IAAtRIHLAAIAANRg");
	var mask_graphics_335 = new cjs.Graphics().p("AlrH/IAAtRIHLAAIAANRg");
	var mask_graphics_336 = new cjs.Graphics().p("AldICIAAtRIHLAAIAANRg");
	var mask_graphics_337 = new cjs.Graphics().p("AlQIFIAAtRIHLAAIAANRg");
	var mask_graphics_338 = new cjs.Graphics().p("AlCIIIAAtRIHLAAIAANRg");
	var mask_graphics_339 = new cjs.Graphics().p("Ak0ILIAAtRIHLAAIAANRg");
	var mask_graphics_340 = new cjs.Graphics().p("AknIOIAAtRIHLAAIAANRg");
	var mask_graphics_341 = new cjs.Graphics().p("AkZIRIAAtRIHLAAIAANRg");
	var mask_graphics_342 = new cjs.Graphics().p("AkMIUIAAtRIHLAAIAANRg");
	var mask_graphics_343 = new cjs.Graphics().p("Aj+IXIAAtRIHLAAIAANRg");
	var mask_graphics_344 = new cjs.Graphics().p("AjwIaIAAtRIHLAAIAANRg");
	var mask_graphics_345 = new cjs.Graphics().p("AjlIdIAAtRIHLAAIAANRg");
	var mask_graphics_346 = new cjs.Graphics().p("AjlIgIAAtRIHLAAIAANRg");
	var mask_graphics_347 = new cjs.Graphics().p("AjlIkIAAtRIHLAAIAANRg");
	var mask_graphics_348 = new cjs.Graphics().p("AjlInIAAtRIHLAAIAANRg");
	var mask_graphics_349 = new cjs.Graphics().p("AjlIqIAAtRIHLAAIAANRg");
	var mask_graphics_350 = new cjs.Graphics().p("AjlItIAAtRIHLAAIAANRg");
	var mask_graphics_351 = new cjs.Graphics().p("AjlIwIAAtRIHLAAIAANRg");
	var mask_graphics_352 = new cjs.Graphics().p("AjlIzIAAtRIHLAAIAANRg");
	var mask_graphics_353 = new cjs.Graphics().p("AjlI2IAAtRIHLAAIAANRg");
	var mask_graphics_354 = new cjs.Graphics().p("AjlI5IAAtRIHLAAIAANRg");
	var mask_graphics_355 = new cjs.Graphics().p("AjlI8IAAtRIHLAAIAANRg");
	var mask_graphics_356 = new cjs.Graphics().p("AjlI/IAAtRIHLAAIAANRg");
	var mask_graphics_357 = new cjs.Graphics().p("AjlJCIAAtRIHLAAIAANRg");
	var mask_graphics_358 = new cjs.Graphics().p("AjlJFIAAtRIHLAAIAANRg");
	var mask_graphics_359 = new cjs.Graphics().p("AjlJJIAAtRIHLAAIAANRg");
	var mask_graphics_360 = new cjs.Graphics().p("AjlJMIAAtRIHLAAIAANRg");
	var mask_graphics_361 = new cjs.Graphics().p("AjlJPIAAtRIHLAAIAANRg");
	var mask_graphics_362 = new cjs.Graphics().p("AjiJSIAAtRIHLAAIAANRg");
	var mask_graphics_363 = new cjs.Graphics().p("AjUJVIAAtRIHLAAIAANRg");
	var mask_graphics_364 = new cjs.Graphics().p("AjGJYIAAtRIHLAAIAANRg");
	var mask_graphics_365 = new cjs.Graphics().p("Ai5JbIAAtRIHLAAIAANRg");
	var mask_graphics_366 = new cjs.Graphics().p("AirJeIAAtRIHLAAIAANRg");
	var mask_graphics_367 = new cjs.Graphics().p("AidJhIAAtRIHLAAIAANRg");
	var mask_graphics_368 = new cjs.Graphics().p("AiQJkIAAtRIHLAAIAANRg");
	var mask_graphics_369 = new cjs.Graphics().p("AiCJnIAAtRIHLAAIAANRg");
	var mask_graphics_370 = new cjs.Graphics().p("Ah1JqIAAtRIHLAAIAANRg");
	var mask_graphics_371 = new cjs.Graphics().p("AhnJuIAAtRIHLAAIAANRg");
	var mask_graphics_372 = new cjs.Graphics().p("AhZJxIAAtRIHLAAIAANRg");
	var mask_graphics_373 = new cjs.Graphics().p("AhMJ0IAAtRIHLAAIAANRg");
	var mask_graphics_374 = new cjs.Graphics().p("Ag+J3IAAtRIHLAAIAANRg");
	var mask_graphics_479 = new cjs.Graphics().p("Ag+J3IAAtRIHLAAIAANRg");
	var mask_graphics_480 = new cjs.Graphics().p("Ag/J0IAAtPIHLAAIAANPg");
	var mask_graphics_481 = new cjs.Graphics().p("AhAJyIAAtNIHLAAIAANNg");
	var mask_graphics_482 = new cjs.Graphics().p("AhBJwIAAtMIHLAAIAANMg");
	var mask_graphics_483 = new cjs.Graphics().p("AhCJtIAAtJIHLAAIAANJg");
	var mask_graphics_484 = new cjs.Graphics().p("AhDJrIAAtHIHLAAIAANHg");
	var mask_graphics_485 = new cjs.Graphics().p("AhEJpIAAtGIHLAAIAANGg");
	var mask_graphics_486 = new cjs.Graphics().p("AhFJmIAAtDIHLAAIAANDg");
	var mask_graphics_487 = new cjs.Graphics().p("AhHJkIAAtCIHLAAIAANCg");
	var mask_graphics_488 = new cjs.Graphics().p("AhIJiIAAtAIHLAAIAANAg");
	var mask_graphics_489 = new cjs.Graphics().p("AhJJfIAAs+IHLAAIAAM+g");
	var mask_graphics_490 = new cjs.Graphics().p("AhKJdIAAs8IHLAAIAAM8g");
	var mask_graphics_491 = new cjs.Graphics().p("AhLJbIAAs7IHLAAIAAM7g");
	var mask_graphics_492 = new cjs.Graphics().p("AhMJYIAAs4IHLAAIAAM4g");
	var mask_graphics_493 = new cjs.Graphics().p("AhNJWIAAs3IHLAAIAAM3g");
	var mask_graphics_494 = new cjs.Graphics().p("AhOJUIAAs1IHLAAIAAM1g");
	var mask_graphics_495 = new cjs.Graphics().p("AhPJRIAAsyIHLAAIAAMyg");
	var mask_graphics_496 = new cjs.Graphics().p("AhQJPIAAsxIHLAAIAAMxg");
	var mask_graphics_497 = new cjs.Graphics().p("AhRJNIAAsvIHLAAIAAMvg");
	var mask_graphics_498 = new cjs.Graphics().p("AhSJKIAAstIHLAAIAAMtg");
	var mask_graphics_499 = new cjs.Graphics().p("AhTJIIAAsrIHLAAIAAMrg");
	var mask_graphics_500 = new cjs.Graphics().p("AhUJGIAAsqIHLAAIAAMqg");
	var mask_graphics_501 = new cjs.Graphics().p("AhVJDIAAsnIHLAAIAAMng");
	var mask_graphics_502 = new cjs.Graphics().p("AhWJBIAAsmIHLAAIAAMmg");
	var mask_graphics_503 = new cjs.Graphics().p("AhXI/IAAskIHLAAIAAMkg");
	var mask_graphics_504 = new cjs.Graphics().p("AhYI8IAAshIHLAAIAAMhg");
	var mask_graphics_505 = new cjs.Graphics().p("AhZI6IAAsgIHLAAIAAMgg");
	var mask_graphics_506 = new cjs.Graphics().p("AhaI4IAAseIHLAAIAAMeg");
	var mask_graphics_507 = new cjs.Graphics().p("AhbI1IAAscIHLAAIAAMcg");
	var mask_graphics_508 = new cjs.Graphics().p("AhcIzIAAsaIHLAAIAAMag");
	var mask_graphics_509 = new cjs.Graphics().p("AhdIxIAAsZIHLAAIAAMZg");
	var mask_graphics_510 = new cjs.Graphics().p("AheIuIAAsWIHLAAIAAMWg");
	var mask_graphics_511 = new cjs.Graphics().p("AhfIsIAAsVIHLAAIAAMVg");
	var mask_graphics_512 = new cjs.Graphics().p("AhgIqIAAsTIHLAAIAAMTg");
	var mask_graphics_513 = new cjs.Graphics().p("AhhInIAAsQIHLAAIAAMQg");
	var mask_graphics_514 = new cjs.Graphics().p("AhiIlIAAsPIHLAAIAAMPg");
	var mask_graphics_515 = new cjs.Graphics().p("AhjIjIAAsNIHLAAIAAMNg");
	var mask_graphics_516 = new cjs.Graphics().p("AhkIgIAAsLIHLAAIAAMLg");
	var mask_graphics_517 = new cjs.Graphics().p("AhmIeIAAsJIHLAAIAAMJg");
	var mask_graphics_518 = new cjs.Graphics().p("AhnIcIAAsIIHLAAIAAMIg");
	var mask_graphics_519 = new cjs.Graphics().p("AhoIZIAAsFIHLAAIAAMFg");
	var mask_graphics_520 = new cjs.Graphics().p("AhpIXIAAsEIHLAAIAAMEg");
	var mask_graphics_521 = new cjs.Graphics().p("AhqIVIAAsCIHLAAIAAMCg");
	var mask_graphics_522 = new cjs.Graphics().p("AhrISIAAsAIHLAAIAAMAg");
	var mask_graphics_523 = new cjs.Graphics().p("AhsIQIAAr+IHLAAIAAL+g");
	var mask_graphics_524 = new cjs.Graphics().p("AhtIOIAAr8IHLAAIAAL8g");
	var mask_graphics_525 = new cjs.Graphics().p("AhuILIAAr6IHLAAIAAL6g");
	var mask_graphics_526 = new cjs.Graphics().p("AhvIJIAAr4IHLAAIAAL4g");
	var mask_graphics_527 = new cjs.Graphics().p("AhwIHIAAr3IHLAAIAAL3g");
	var mask_graphics_528 = new cjs.Graphics().p("AhxIEIAAr0IHLAAIAAL0g");
	var mask_graphics_529 = new cjs.Graphics().p("AhyICIAArzIHLAAIAALzg");
	var mask_graphics_530 = new cjs.Graphics().p("AhzIAIAArxIHLAAIAALxg");
	var mask_graphics_531 = new cjs.Graphics().p("Ah0H9IAArvIHLAAIAALvg");
	var mask_graphics_532 = new cjs.Graphics().p("Ah1H8IAArtIHLAAIAALtg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:34.2,y:50.8}).wait(40).to({graphics:mask_graphics_40,x:34.2,y:50.8}).wait(1).to({graphics:mask_graphics_41,x:34.1,y:51}).wait(1).to({graphics:mask_graphics_42,x:34,y:51.2}).wait(1).to({graphics:mask_graphics_43,x:34,y:51.4}).wait(1).to({graphics:mask_graphics_44,x:33.9,y:51.7}).wait(1).to({graphics:mask_graphics_45,x:33.8,y:51.9}).wait(1).to({graphics:mask_graphics_46,x:33.8,y:52.1}).wait(1).to({graphics:mask_graphics_47,x:33.7,y:52.3}).wait(1).to({graphics:mask_graphics_48,x:33.6,y:52.5}).wait(1).to({graphics:mask_graphics_49,x:33.5,y:52.8}).wait(1).to({graphics:mask_graphics_50,x:33.5,y:53}).wait(1).to({graphics:mask_graphics_51,x:33.4,y:53.2}).wait(1).to({graphics:mask_graphics_52,x:33.3,y:53.4}).wait(1).to({graphics:mask_graphics_53,x:33.3,y:53.6}).wait(1).to({graphics:mask_graphics_54,x:33.2,y:53.9}).wait(1).to({graphics:mask_graphics_55,x:33.1,y:54.1}).wait(1).to({graphics:mask_graphics_56,x:33.1,y:54.3}).wait(1).to({graphics:mask_graphics_57,x:33,y:54.5}).wait(1).to({graphics:mask_graphics_58,x:32.9,y:54.8}).wait(1).to({graphics:mask_graphics_59,x:32.8,y:55}).wait(1).to({graphics:mask_graphics_60,x:32.8,y:55.2}).wait(1).to({graphics:mask_graphics_61,x:32.7,y:55.4}).wait(1).to({graphics:mask_graphics_62,x:32.6,y:55.6}).wait(1).to({graphics:mask_graphics_63,x:32.6,y:55.9}).wait(1).to({graphics:mask_graphics_64,x:32.5,y:56.1}).wait(1).to({graphics:mask_graphics_65,x:32.4,y:56.3}).wait(1).to({graphics:mask_graphics_66,x:32.4,y:56.5}).wait(1).to({graphics:mask_graphics_67,x:32.3,y:56.7}).wait(1).to({graphics:mask_graphics_68,x:32.2,y:57}).wait(1).to({graphics:mask_graphics_69,x:32.2,y:57.2}).wait(1).to({graphics:mask_graphics_70,x:32.1,y:57.4}).wait(1).to({graphics:mask_graphics_71,x:32,y:57.6}).wait(1).to({graphics:mask_graphics_72,x:31.9,y:57.8}).wait(1).to({graphics:mask_graphics_73,x:31.9,y:58.1}).wait(1).to({graphics:mask_graphics_74,x:31.8,y:58.3}).wait(1).to({graphics:mask_graphics_75,x:31.7,y:58.5}).wait(1).to({graphics:mask_graphics_76,x:31.7,y:58.7}).wait(1).to({graphics:mask_graphics_77,x:31.6,y:58.9}).wait(1).to({graphics:mask_graphics_78,x:31.5,y:59.2}).wait(1).to({graphics:mask_graphics_79,x:31.5,y:59.4}).wait(1).to({graphics:mask_graphics_80,x:31.4,y:59.6}).wait(1).to({graphics:mask_graphics_81,x:31.3,y:59.8}).wait(1).to({graphics:mask_graphics_82,x:31.2,y:60.1}).wait(1).to({graphics:mask_graphics_83,x:31.2,y:60.3}).wait(1).to({graphics:mask_graphics_84,x:30.1,y:59.9}).wait(1).to({graphics:mask_graphics_85,x:29,y:59.6}).wait(1).to({graphics:mask_graphics_86,x:27.8,y:59.2}).wait(1).to({graphics:mask_graphics_87,x:26.7,y:58.9}).wait(1).to({graphics:mask_graphics_88,x:25.6,y:58.6}).wait(1).to({graphics:mask_graphics_89,x:24.5,y:58.2}).wait(1).to({graphics:mask_graphics_90,x:23.4,y:57.9}).wait(1).to({graphics:mask_graphics_91,x:21.6,y:57.5}).wait(1).to({graphics:mask_graphics_92,x:19.4,y:57.2}).wait(1).to({graphics:mask_graphics_93,x:17.1,y:56.8}).wait(1).to({graphics:mask_graphics_94,x:14.9,y:56.5}).wait(1).to({graphics:mask_graphics_95,x:12.7,y:56.1}).wait(1).to({graphics:mask_graphics_96,x:10.5,y:55.8}).wait(1).to({graphics:mask_graphics_97,x:8.2,y:55.5}).wait(1).to({graphics:mask_graphics_98,x:6,y:55.1}).wait(1).to({graphics:mask_graphics_99,x:3.8,y:54.8}).wait(1).to({graphics:mask_graphics_100,x:1.6,y:54.4}).wait(1).to({graphics:mask_graphics_101,x:-0.6,y:54.1}).wait(1).to({graphics:mask_graphics_102,x:-2.9,y:53.7}).wait(1).to({graphics:mask_graphics_103,x:-5.1,y:53.4}).wait(1).to({graphics:mask_graphics_104,x:-7.3,y:53}).wait(1).to({graphics:mask_graphics_105,x:-9.5,y:52.7}).wait(1).to({graphics:mask_graphics_106,x:-11.8,y:52.4}).wait(1).to({graphics:mask_graphics_107,x:-14,y:52}).wait(1).to({graphics:mask_graphics_108,x:-16.2,y:51.7}).wait(1).to({graphics:mask_graphics_109,x:-18.4,y:51.3}).wait(1).to({graphics:mask_graphics_110,x:-20.6,y:51}).wait(1).to({graphics:mask_graphics_111,x:-22.9,y:50.6}).wait(1).to({graphics:mask_graphics_112,x:-24,y:50.3}).wait(1).to({graphics:mask_graphics_113,x:-25.2,y:49.9}).wait(1).to({graphics:mask_graphics_114,x:-26.3,y:49.6}).wait(1).to({graphics:mask_graphics_115,x:-27.4,y:49.3}).wait(1).to({graphics:mask_graphics_116,x:-28.5,y:48.9}).wait(1).to({graphics:mask_graphics_117,x:-29.6,y:48.6}).wait(1).to({graphics:mask_graphics_118,x:-30.7,y:48.2}).wait(1).to({graphics:mask_graphics_119,x:-31.8,y:47.9}).wait(1).to({graphics:mask_graphics_120,x:-32.9,y:47.5}).wait(1).to({graphics:mask_graphics_121,x:-34,y:47.2}).wait(1).to({graphics:mask_graphics_122,x:-35.2,y:46.8}).wait(1).to({graphics:mask_graphics_123,x:-36.3,y:46.5}).wait(1).to({graphics:mask_graphics_124,x:-37.4,y:46.2}).wait(1).to({graphics:mask_graphics_125,x:-38.5,y:45.8}).wait(1).to({graphics:mask_graphics_126,x:-39.6,y:45.5}).wait(1).to({graphics:mask_graphics_127,x:-40.7,y:45.1}).wait(1).to({graphics:mask_graphics_128,x:-41.8,y:44.8}).wait(5).to({graphics:mask_graphics_133,x:-41.8,y:44.8}).wait(1).to({graphics:mask_graphics_134,x:-41.8,y:45.6}).wait(1).to({graphics:mask_graphics_135,x:-41.8,y:46.4}).wait(1).to({graphics:mask_graphics_136,x:-41.8,y:47.2}).wait(1).to({graphics:mask_graphics_137,x:-41.8,y:48}).wait(1).to({graphics:mask_graphics_138,x:-41.8,y:48.8}).wait(1).to({graphics:mask_graphics_139,x:-41.8,y:49.6}).wait(1).to({graphics:mask_graphics_140,x:-41.8,y:50.5}).wait(1).to({graphics:mask_graphics_141,x:-41.8,y:51.3}).wait(1).to({graphics:mask_graphics_142,x:-41.8,y:52.1}).wait(1).to({graphics:mask_graphics_143,x:-41.8,y:52.9}).wait(1).to({graphics:mask_graphics_144,x:-41.8,y:53.7}).wait(1).to({graphics:mask_graphics_145,x:-41.8,y:54.5}).wait(1).to({graphics:mask_graphics_146,x:-41.8,y:55.3}).wait(1).to({graphics:mask_graphics_147,x:-41.8,y:56.1}).wait(1).to({graphics:mask_graphics_148,x:-41.8,y:57}).wait(1).to({graphics:mask_graphics_149,x:-41.8,y:57.8}).wait(1).to({graphics:mask_graphics_150,x:-41.8,y:58.6}).wait(1).to({graphics:mask_graphics_151,x:-41.8,y:59.4}).wait(1).to({graphics:mask_graphics_152,x:-41.8,y:60.2}).wait(1).to({graphics:mask_graphics_153,x:-41.8,y:61}).wait(1).to({graphics:mask_graphics_154,x:-41.8,y:61.8}).wait(1).to({graphics:mask_graphics_155,x:-41.8,y:62.6}).wait(1).to({graphics:mask_graphics_156,x:-41.8,y:63.4}).wait(1).to({graphics:mask_graphics_157,x:-41.8,y:64.3}).wait(1).to({graphics:mask_graphics_158,x:-41.8,y:65.1}).wait(1).to({graphics:mask_graphics_159,x:-41.8,y:65.9}).wait(1).to({graphics:mask_graphics_160,x:-41.8,y:66.7}).wait(1).to({graphics:mask_graphics_161,x:-41.8,y:67.5}).wait(1).to({graphics:mask_graphics_162,x:-41.8,y:68.3}).wait(1).to({graphics:mask_graphics_163,x:-41.8,y:69.1}).wait(1).to({graphics:mask_graphics_164,x:-41.8,y:69.9}).wait(1).to({graphics:mask_graphics_165,x:-41.8,y:70.8}).wait(1).to({graphics:mask_graphics_166,x:-41.8,y:71.6}).wait(1).to({graphics:mask_graphics_167,x:-41.8,y:72.4}).wait(1).to({graphics:mask_graphics_168,x:-41.8,y:73.2}).wait(1).to({graphics:mask_graphics_169,x:-41.8,y:74}).wait(1).to({graphics:mask_graphics_170,x:-41.8,y:74.8}).wait(1).to({graphics:mask_graphics_171,x:-41.8,y:75.6}).wait(1).to({graphics:mask_graphics_172,x:-41.8,y:76.4}).wait(1).to({graphics:mask_graphics_173,x:-41.8,y:77.3}).wait(1).to({graphics:mask_graphics_174,x:-41.8,y:78.1}).wait(1).to({graphics:mask_graphics_175,x:-41.8,y:78.9}).wait(1).to({graphics:mask_graphics_176,x:-41.8,y:79.7}).wait(1).to({graphics:mask_graphics_177,x:-41.8,y:80.5}).wait(1).to({graphics:mask_graphics_178,x:-41.8,y:81.3}).wait(1).to({graphics:mask_graphics_179,x:-41.8,y:82.1}).wait(1).to({graphics:mask_graphics_180,x:-41.8,y:82.9}).wait(1).to({graphics:mask_graphics_181,x:-41.8,y:83.7}).wait(1).to({graphics:mask_graphics_182,x:-41.8,y:84.6}).wait(1).to({graphics:mask_graphics_183,x:-41.8,y:85.4}).wait(1).to({graphics:mask_graphics_184,x:-41.8,y:86.2}).wait(1).to({graphics:mask_graphics_185,x:-41.8,y:87}).wait(1).to({graphics:mask_graphics_186,x:-41.8,y:87.8}).wait(1).to({graphics:mask_graphics_187,x:-41.8,y:88.6}).wait(1).to({graphics:mask_graphics_188,x:-41.8,y:89.4}).wait(1).to({graphics:mask_graphics_189,x:-41.8,y:90.2}).wait(1).to({graphics:mask_graphics_190,x:-41.8,y:91.1}).wait(1).to({graphics:mask_graphics_191,x:-41.8,y:91.9}).wait(1).to({graphics:mask_graphics_192,x:-41.8,y:92.7}).wait(1).to({graphics:mask_graphics_193,x:-41.8,y:93.5}).wait(1).to({graphics:mask_graphics_194,x:-41.8,y:94.3}).wait(1).to({graphics:mask_graphics_195,x:-41.8,y:95.1}).wait(1).to({graphics:mask_graphics_196,x:-41.8,y:95.9}).wait(1).to({graphics:mask_graphics_197,x:-41.8,y:96.7}).wait(1).to({graphics:mask_graphics_198,x:-41.8,y:97.5}).wait(1).to({graphics:mask_graphics_199,x:-41.8,y:98.4}).wait(1).to({graphics:mask_graphics_200,x:-41.8,y:99.2}).wait(1).to({graphics:mask_graphics_201,x:-41.8,y:99.8}).wait(49).to({graphics:mask_graphics_250,x:-41.8,y:99.8}).wait(1).to({graphics:mask_graphics_251,x:-41.8,y:98.5}).wait(1).to({graphics:mask_graphics_252,x:-41.8,y:97.2}).wait(1).to({graphics:mask_graphics_253,x:-41.8,y:95.9}).wait(1).to({graphics:mask_graphics_254,x:-41.8,y:94.5}).wait(1).to({graphics:mask_graphics_255,x:-41.8,y:93.2}).wait(1).to({graphics:mask_graphics_256,x:-41.8,y:91.9}).wait(1).to({graphics:mask_graphics_257,x:-41.8,y:90.6}).wait(1).to({graphics:mask_graphics_258,x:-41.8,y:89.3}).wait(1).to({graphics:mask_graphics_259,x:-41.8,y:88}).wait(1).to({graphics:mask_graphics_260,x:-41.8,y:86.6}).wait(1).to({graphics:mask_graphics_261,x:-41.8,y:85.3}).wait(1).to({graphics:mask_graphics_262,x:-41.8,y:84}).wait(1).to({graphics:mask_graphics_263,x:-41.8,y:82.7}).wait(1).to({graphics:mask_graphics_264,x:-41.8,y:81.4}).wait(1).to({graphics:mask_graphics_265,x:-41.8,y:80.1}).wait(1).to({graphics:mask_graphics_266,x:-41.8,y:78.8}).wait(1).to({graphics:mask_graphics_267,x:-41.8,y:77.4}).wait(1).to({graphics:mask_graphics_268,x:-41.8,y:76.1}).wait(1).to({graphics:mask_graphics_269,x:-41.8,y:74.8}).wait(1).to({graphics:mask_graphics_270,x:-41.8,y:73.5}).wait(1).to({graphics:mask_graphics_271,x:-41.8,y:72.2}).wait(1).to({graphics:mask_graphics_272,x:-41.8,y:70.9}).wait(1).to({graphics:mask_graphics_273,x:-41.8,y:69.6}).wait(1).to({graphics:mask_graphics_274,x:-41.8,y:68.2}).wait(1).to({graphics:mask_graphics_275,x:-41.8,y:66.9}).wait(1).to({graphics:mask_graphics_276,x:-41.8,y:65.6}).wait(1).to({graphics:mask_graphics_277,x:-41.8,y:64.3}).wait(1).to({graphics:mask_graphics_278,x:-41.8,y:63}).wait(1).to({graphics:mask_graphics_279,x:-41.8,y:61.7}).wait(1).to({graphics:mask_graphics_280,x:-41.8,y:60.3}).wait(1).to({graphics:mask_graphics_281,x:-41.8,y:59}).wait(1).to({graphics:mask_graphics_282,x:-41.8,y:57.7}).wait(1).to({graphics:mask_graphics_283,x:-41.8,y:56.4}).wait(1).to({graphics:mask_graphics_284,x:-41.8,y:55.1}).wait(1).to({graphics:mask_graphics_285,x:-41.8,y:53.8}).wait(1).to({graphics:mask_graphics_286,x:-41.8,y:52.5}).wait(1).to({graphics:mask_graphics_287,x:-41.8,y:51.1}).wait(1).to({graphics:mask_graphics_288,x:-41.8,y:49.8}).wait(43).to({graphics:mask_graphics_331,x:-41.8,y:49.8}).wait(1).to({graphics:mask_graphics_332,x:-40.5,y:50.1}).wait(1).to({graphics:mask_graphics_333,x:-39.1,y:50.4}).wait(1).to({graphics:mask_graphics_334,x:-37.7,y:50.7}).wait(1).to({graphics:mask_graphics_335,x:-36.4,y:51.1}).wait(1).to({graphics:mask_graphics_336,x:-35,y:51.4}).wait(1).to({graphics:mask_graphics_337,x:-33.7,y:51.7}).wait(1).to({graphics:mask_graphics_338,x:-32.3,y:52}).wait(1).to({graphics:mask_graphics_339,x:-30.9,y:52.3}).wait(1).to({graphics:mask_graphics_340,x:-29.6,y:52.6}).wait(1).to({graphics:mask_graphics_341,x:-28.2,y:52.9}).wait(1).to({graphics:mask_graphics_342,x:-26.9,y:53.2}).wait(1).to({graphics:mask_graphics_343,x:-25.5,y:53.5}).wait(1).to({graphics:mask_graphics_344,x:-24.1,y:53.8}).wait(1).to({graphics:mask_graphics_345,x:-22.6,y:54.1}).wait(1).to({graphics:mask_graphics_346,x:-19.8,y:54.4}).wait(1).to({graphics:mask_graphics_347,x:-17.1,y:54.8}).wait(1).to({graphics:mask_graphics_348,x:-14.4,y:55.1}).wait(1).to({graphics:mask_graphics_349,x:-11.7,y:55.4}).wait(1).to({graphics:mask_graphics_350,x:-9,y:55.7}).wait(1).to({graphics:mask_graphics_351,x:-6.2,y:56}).wait(1).to({graphics:mask_graphics_352,x:-3.5,y:56.3}).wait(1).to({graphics:mask_graphics_353,x:-0.8,y:56.6}).wait(1).to({graphics:mask_graphics_354,x:1.9,y:56.9}).wait(1).to({graphics:mask_graphics_355,x:4.7,y:57.2}).wait(1).to({graphics:mask_graphics_356,x:7.4,y:57.5}).wait(1).to({graphics:mask_graphics_357,x:10.1,y:57.8}).wait(1).to({graphics:mask_graphics_358,x:12.8,y:58.1}).wait(1).to({graphics:mask_graphics_359,x:15.5,y:58.5}).wait(1).to({graphics:mask_graphics_360,x:18.3,y:58.8}).wait(1).to({graphics:mask_graphics_361,x:21,y:59.1}).wait(1).to({graphics:mask_graphics_362,x:23.3,y:59.4}).wait(1).to({graphics:mask_graphics_363,x:24.7,y:59.7}).wait(1).to({graphics:mask_graphics_364,x:26.1,y:60}).wait(1).to({graphics:mask_graphics_365,x:27.4,y:60.3}).wait(1).to({graphics:mask_graphics_366,x:28.8,y:60.6}).wait(1).to({graphics:mask_graphics_367,x:30.2,y:60.9}).wait(1).to({graphics:mask_graphics_368,x:31.5,y:61.2}).wait(1).to({graphics:mask_graphics_369,x:32.9,y:61.5}).wait(1).to({graphics:mask_graphics_370,x:34.2,y:61.8}).wait(1).to({graphics:mask_graphics_371,x:35.6,y:62.2}).wait(1).to({graphics:mask_graphics_372,x:37,y:62.5}).wait(1).to({graphics:mask_graphics_373,x:38.3,y:62.8}).wait(1).to({graphics:mask_graphics_374,x:39.7,y:63.1}).wait(105).to({graphics:mask_graphics_479,x:39.7,y:63.1}).wait(1).to({graphics:mask_graphics_480,x:39.6,y:62.8}).wait(1).to({graphics:mask_graphics_481,x:39.5,y:62.6}).wait(1).to({graphics:mask_graphics_482,x:39.4,y:62.4}).wait(1).to({graphics:mask_graphics_483,x:39.3,y:62.1}).wait(1).to({graphics:mask_graphics_484,x:39.2,y:61.9}).wait(1).to({graphics:mask_graphics_485,x:39.1,y:61.7}).wait(1).to({graphics:mask_graphics_486,x:39,y:61.4}).wait(1).to({graphics:mask_graphics_487,x:38.8,y:61.2}).wait(1).to({graphics:mask_graphics_488,x:38.7,y:61}).wait(1).to({graphics:mask_graphics_489,x:38.6,y:60.7}).wait(1).to({graphics:mask_graphics_490,x:38.5,y:60.5}).wait(1).to({graphics:mask_graphics_491,x:38.4,y:60.3}).wait(1).to({graphics:mask_graphics_492,x:38.3,y:60}).wait(1).to({graphics:mask_graphics_493,x:38.2,y:59.8}).wait(1).to({graphics:mask_graphics_494,x:38.1,y:59.6}).wait(1).to({graphics:mask_graphics_495,x:38,y:59.3}).wait(1).to({graphics:mask_graphics_496,x:37.9,y:59.1}).wait(1).to({graphics:mask_graphics_497,x:37.8,y:58.9}).wait(1).to({graphics:mask_graphics_498,x:37.7,y:58.6}).wait(1).to({graphics:mask_graphics_499,x:37.6,y:58.4}).wait(1).to({graphics:mask_graphics_500,x:37.5,y:58.2}).wait(1).to({graphics:mask_graphics_501,x:37.4,y:57.9}).wait(1).to({graphics:mask_graphics_502,x:37.3,y:57.7}).wait(1).to({graphics:mask_graphics_503,x:37.2,y:57.5}).wait(1).to({graphics:mask_graphics_504,x:37.1,y:57.2}).wait(1).to({graphics:mask_graphics_505,x:37,y:57}).wait(1).to({graphics:mask_graphics_506,x:36.9,y:56.8}).wait(1).to({graphics:mask_graphics_507,x:36.8,y:56.5}).wait(1).to({graphics:mask_graphics_508,x:36.7,y:56.3}).wait(1).to({graphics:mask_graphics_509,x:36.6,y:56.1}).wait(1).to({graphics:mask_graphics_510,x:36.5,y:55.8}).wait(1).to({graphics:mask_graphics_511,x:36.4,y:55.6}).wait(1).to({graphics:mask_graphics_512,x:36.3,y:55.4}).wait(1).to({graphics:mask_graphics_513,x:36.2,y:55.1}).wait(1).to({graphics:mask_graphics_514,x:36.1,y:54.9}).wait(1).to({graphics:mask_graphics_515,x:36,y:54.7}).wait(1).to({graphics:mask_graphics_516,x:35.9,y:54.4}).wait(1).to({graphics:mask_graphics_517,x:35.7,y:54.2}).wait(1).to({graphics:mask_graphics_518,x:35.6,y:54}).wait(1).to({graphics:mask_graphics_519,x:35.5,y:53.7}).wait(1).to({graphics:mask_graphics_520,x:35.4,y:53.5}).wait(1).to({graphics:mask_graphics_521,x:35.3,y:53.3}).wait(1).to({graphics:mask_graphics_522,x:35.2,y:53}).wait(1).to({graphics:mask_graphics_523,x:35.1,y:52.8}).wait(1).to({graphics:mask_graphics_524,x:35,y:52.6}).wait(1).to({graphics:mask_graphics_525,x:34.9,y:52.3}).wait(1).to({graphics:mask_graphics_526,x:34.8,y:52.1}).wait(1).to({graphics:mask_graphics_527,x:34.7,y:51.9}).wait(1).to({graphics:mask_graphics_528,x:34.6,y:51.6}).wait(1).to({graphics:mask_graphics_529,x:34.5,y:51.4}).wait(1).to({graphics:mask_graphics_530,x:34.4,y:51.2}).wait(1).to({graphics:mask_graphics_531,x:34.3,y:50.9}).wait(1).to({graphics:mask_graphics_532,x:34.2,y:50.8}).wait(35));

	// Layer_4
	this.instance_4 = new lib.Symbol29();
	this.instance_4.parent = this;
	this.instance_4.setTransform(44,65.3,1,1,0,0,0,5.5,30.3);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(40).to({regX:5.6,rotation:-0.2,x:37.7,y:84.9},43,cjs.Ease.quadInOut).to({x:-61.4,y:52.4},45,cjs.Ease.quadInOut).wait(203).to({x:55.6,y:78.9},43,cjs.Ease.quadInOut).wait(105).to({regX:5.5,rotation:0,x:44,y:65.3},53,cjs.Ease.quadInOut).wait(35));

	// Layer_6
	this.instance_5 = new lib.Symbol31();
	this.instance_5.parent = this;
	this.instance_5.setTransform(237.2,93.1,1,1,22.2,0,0,101.8,40.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(40).to({regX:101.7,rotation:16.5,x:237,y:93},43,cjs.Ease.quadInOut).wait(396).to({regX:101.8,rotation:22.2,x:237.2,y:93.1},53,cjs.Ease.quadInOut).wait(35));

	// Layer_7
	this.instance_6 = new lib.Symbol30();
	this.instance_6.parent = this;
	this.instance_6.setTransform(100.4,36.3,1,1,22.2,0,0,54.7,6.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(40).to({rotation:16.5,x:95.3,y:50.2},43,cjs.Ease.quadInOut).to({x:-11.2,y:18.2},45,cjs.Ease.quadInOut).wait(203).to({regY:6.2,scaleX:0.43,x:79.4,y:45.2},43,cjs.Ease.quadInOut).wait(105).to({regY:6.1,scaleX:1,rotation:22.2,x:100.4,y:36.3},53,cjs.Ease.quadInOut).wait(35));

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#767D93").s().p("Ag2A2QgtgTgkgiIAehJIDxBIIgaBAQgkAJgeAAQgyAAgwgTg");
	this.shape.setTransform(228,118.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#40516C").ss(3).p("AhRiCIhqEIIEuAAIBLi4g");
	this.shape_1.setTransform(224.9,122.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9AA0B5").s().p("Ai8CFIBqkJIEPBQIhLC5g");
	this.shape_2.setTransform(225,122.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#40516C").s().p("AldAtIAAgoQAAgTAPgPQAPgPAUAAIJXAAQAVAAAOAPQAPAPAAATIAAAog");
	this.shape_3.setTransform(222.5,140);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(567));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-134.2,-6.4,410.5,314.7);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(5,25.3,1,1,0,0,0,5,25.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:25.2,scaleX:1.08,scaleY:1.08,x:5.1},30).to({regY:25.3,scaleX:1,scaleY:1,x:5},30).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,10.1,50.6);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol24("single",0);
	this.instance.parent = this;
	this.instance.setTransform(46.6,70.4,1,1,75.8,0,0,5.3,25.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(190));

	// Layer_5
	this.instance_1 = new lib.Symbol24("single",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(43.1,54.5,1,1,55.1,0,0,5.2,25.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(190));

	// Layer_4
	this.instance_2 = new lib.Symbol24("single",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(35.4,41.3,1,1,36,0,0,5.1,25.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(190));

	// Layer_3
	this.instance_3 = new lib.Symbol24("single",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(19.8,30.1,1,1,10,0,0,5.1,25.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(190));

	// Layer_2
	this.instance_4 = new lib.Symbol24("single",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(5,25.3,1,1,0,0,0,5,25.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(190));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,71.6,75.7);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(34.4,22.5,1,1,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:20.2,scaleX:1,scaleY:1,guide:{path:[34.5,22.5,206.9,22.1,379.3,21.8,380.6,21.8,381.9,22]}},208,cjs.Ease.none).to({regY:20.3,scaleX:1,scaleY:1,rotation:75,guide:{path:[382,22.1,389.6,23.2,397.6,30.7,407.6,40.2,409,52.6,410.5,65.7,416.4,98.5,418.4,109.7,419.7,117.3]}},33,cjs.Ease.get(-0.56)).wait(63));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,0.7,71.8,42);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(23.8,23.9,1,1,0,0,0,4.3,9.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-84.7},85,cjs.Ease.quadInOut).to({rotation:0},88,cjs.Ease.quadInOut).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#40516C").s().p("Ag7FNIAAilQhQgUg3hAQg7hHAAhbQAAhpBKhLQBLhKBogBQBpABBKBKQBLBLAABpQAABbg7BHQg3BAhRAUIAAClgAiejtQhBBCAABdQAABQA0A/QA0A8BOAQIANACIAACgIA5AAIAAigIANgCQBOgQAzg8QA1g/AAhQQAAhdhBhCQhChBhdAAQhcAAhCBBg");
	this.shape.setTransform(23.9,31.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(174));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhyCGQglgogFg3IAAgGIA7AAQAEAAACgCQADgCAAgEQAAgEgDgCQgCgCgEgBIg7AAIAAgEQAEgyAigoIADgEIAqAqQACACAEAAQAEAAACgCQADgCAAgEQAAgEgDgDIgqgpIAFgEQAmghAzgFIAGAAIAAA8QAAADACACQACADAEAAQADAAADgDQACgCAAgDIAAg8IAGAAQAyAFAoAhIAEAEIgqApQgCADAAAEQAAAEACACIAAABQADACADgBQAEAAACgCIAqgqIAEAEQASAVAJAaIABADIkNClg");
	this.shape_1.setTransform(23.4,21.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF8B9").s().p("AAACiQgqAAglgVIAAAAQgMgGgOgMQgygrgFhCIgBgFIA7AAQAEgBADgCQACgDAAgDQAAgCgCgDQgDgCgEAAIg7AAIABgGQADgyAigoIADgEIAqAqQADADAEAAQADgBACgCQADgDAAgDQAAgEgDgDIgpgpIAEgEQAmghAzgEIAGgBIAAA7QAAAEACACIABAAQACADADAAQADAAADgDQACgCABgEIAAg7IAFABQAzAEAnAhIAFADIgrAqQgCADAAAEQAAADACADIAAAAQADADAEAAQADgBACgCIAqgqIAEAEQAhAmAFA0IAAAGIg8AAQgDgBgDADQgCADAAACQAAADACADQADADADAAIA8AAIAAAFQgHBDgyAqIAAAAQgNAOgRAHIABAAQgkASgoAAIAAAAg");
	this.shape_2.setTransform(23.9,23.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#94DEA8").s().p("AilCCQgcg0AAg7QAAheBEhDQBDhDBeAAQAtgBAqASQAnAQAgAeQg7gnhFAAQheAAhDBDQhDBEAABeQAAAwATAsQATArAiAgQgvgggcgxg");
	this.shape_3.setTransform(20.3,22);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#449670").s().p("AgiAbIAAg3IBFAAIAAA3QgTACgQAAQgPAAgTgCg");
	this.shape_4.setTransform(23.9,49.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#40516C").s().p("Ag6CEIAAkHIB1AAIAAEHgAgcBnIA5AAIAAjNIg5AAg");
	this.shape_5.setTransform(23.9,52);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6BBD9C").s().p("AgcBnIAAjNIA5AAIAADNg");
	this.shape_6.setTransform(23.9,52);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#40516C").s().p("AizCzQhKhKAAhpQAAhoBKhLQBLhKBoAAQBpAABKBKQBLBLAABoQAABphLBKQhKBLhpAAQhoAAhLhLgAieieQhBBCAABcQAABdBBBCQBCBBBcAAQBdAABChBQBBhCAAhdQAAhchBhCQhChBhdAAQhcAAhCBBgAiBCDQg2g3AAhMQAAhLA2g2QA2g2BLAAQBMAAA3A2QA2A2AABLQAABMg2A3Qg3A2hMAAQhLAAg2g2gAihAOQAFA3AlAoIACADIENilIAAgDQgKgagSgVIgEgEIgpAqQgDACgEAAQgDABgDgCIAAgBQgCgCAAgEQAAgEACgDIAqgpIgDgEQgoghgzgFIgFAAIAAA8QgBADgCACQgCADgEAAQgCAAgEgDQgCgCAAgDIAAg8IgGAAQgyAFgnAhIgEAEIAqApQACADAAAEQAAAEgCACQgDACgEAAQgDAAgDgCIgqgqIgDAEQgiAogEAzIAAAEIA8AAQADABACACQADACAAADQAAAEgDACQgCACgDABIg8AAg");
	this.shape_7.setTransform(23.9,23.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6BBD9C").s().p("AieCfQhBhCAAhdQAAhcBBhCQBChBBcAAQBdAABCBBQBBBCAABcQAABdhBBCQhCBBhdAAQhcAAhChBgAiBiBQg2A2AABLQAABMA2A3QA2A2BLAAQBMAAA3g2QA2g3AAhMQAAhLg2g2Qg3g2hMAAQhLAAg2A2g");
	this.shape_8.setTransform(23.9,23.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(174));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,50.8,66.7);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(39.3,39.3,1,1,0,0,0,7.3,15.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:15.5,x:39.4},5).to({rotation:0,x:39.3},5).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").p("AigBAIhZAAQAIBXA6BAIGwkIQgPgogdgiIg+A/QgHAGgJAAQgJAAgHgGQgHgHAAgJQAAgJAHgHIA/g/Qg+g0hSgIIAABZQAAAKgGAGQgHAHgJAAQgJAAgGgHQgHgGAAgKIAAhZQhRAIg/A0IA/A/QAHAHAAAJQAAAJgHAHQgGAGgKAAQgJAAgHgGIg+g/Qg1A+gHBQIBZAAQAJAAAHAHQAGAGAAAKQAAAJgGAHQgHAGgJAAg");
	this.shape.setTransform(38.5,35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Aj4BAIBYAAQAKABAGgHQAHgGAAgKQAAgJgHgHQgGgHgKAAIhYAAQAGhPA1g/IA/A/QAHAGAJAAQAJAAAHgGQAHgHAAgJQAAgJgHgHIg/g/QA/g0BRgIIAABZQAAAKAGAHQAHAGAIAAQAJAAAHgGQAHgHAAgKIAAhZQBRAIA+A0Ig/A/QgGAHAAAJQAAAJAGAHQAHAGAJAAQAKAAAGgGIA/g/QAdAiAOApImvEHQg7hAgHhXg");
	this.shape_1.setTransform(38.5,35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#E1F5F8").p("AkAAWIBYAAQAKAAAGgGQAHgHAAgJQAAgJgHgGQgGgHgKAAIhYAAQAGhRA1g+IA/A/QAHAGAJAAQAJAAAHgGQAHgHAAgJQAAgJgHgHIg/g/QA/g0BRgIIAABZQAAAKAGAGQAHAHAIAAQAJAAAHgHQAHgGAAgKIAAhZQBRAIA+A0Ig/A/QgGAHAAAJQAAAJAGAHQAHAGAJAAQAKAAAGgGIA/g/QA1A+AGBRIhYAAQgKAAgGAHQgHAGAAAJQAAAJAHAHQAGAGAKAAIBYAAQgEA0gYAtQgXAsgmAhQgVAVgaALQg5AehAAAQhDAAg8giIAAAAQgSgKgWgSQgmghgXgsQgYgtgEg0g");
	this.shape_2.setTransform(39.3,39.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E1F5F8").s().p("Ah/DgIAAgBQgSgJgWgTQgmgggXgsQgYgtgEg0IBYAAQAKAAAGgHQAHgGAAgJQAAgJgHgGQgGgHgKAAIhYAAQAGhRA1g+IA/A/QAHAGAJAAQAJAAAHgGQAHgHAAgKQAAgJgHgHIg/g/QA/g0BRgHIAABZQAAAJAGAHQAHAGAIAAQAJAAAHgGQAHgHAAgJIAAhZQBRAHA+A0Ig/A/QgGAHAAAJQAAAKAGAHQAHAGAJAAQAKAAAGgGIA/g/QA1A+AGBRIhYAAQgKAAgGAHQgHAGAAAJQAAAJAHAGQAGAHAKAAIBYAAQgEA0gYAtQgXAsgmAhQgVAVgaALQg5AehAAAQhDAAg8gig");
	this.shape_3.setTransform(39.3,39.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#40516C").s().p("AjVDXQhZhZAAh+QAAh8BZhaQBZhZB8AAQB9AABZBZQBZBaAAB8QAAB+hZBZQhYBYh+ABQh8gBhZhYg");
	this.shape_4.setTransform(39.3,39.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F5BDAD").s().p("AkQDVQguhVAAhgQAAicBvhuQBvhvCbAAQBKAABEAcQBCAaA0AxQhfg/hzAAQibAAhvBvQhvBuAACcQAABPAgBJQAfBFA3A1QhNg0gthRg");
	this.shape_5.setTransform(33.4,36.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#40516C").ss(3).p("AGJAAQAACjhzBzQhzBzijAAQihAAh0hzQhzhzAAijQAAiiBzhzQB0hzChAAQCjAABzBzQBzBzAACig");
	this.shape_6.setTransform(39.3,39.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F27A75").s().p("AkVEWQhzhzAAijQAAiiBzhzQB0hzChAAQCjAABzBzQBzBzAACiQAACjhzBzQhzBzijAAQihAAh0hzg");
	this.shape_7.setTransform(39.3,39.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E25656").s().p("Ag5ArIAAhaIBzAAIAABaQgdAFgdAAQgcAAgdgFg");
	this.shape_8.setTransform(39.3,81.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#40516C").ss(3).p("ABJDCIiRAAIAAmCICRAAg");
	this.shape_9.setTransform(39.3,85.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F27A75").s().p("AhIDCIAAmCICRAAIAAGCg");
	this.shape_10.setTransform(39.3,85.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.5,-1.5,81.6,107.8);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").ss(3).p("AgTjtQAHgCAMAAQBZAABCA4QAlAfAWAqQAWAqAEAwQAIBjhABLQhABMhjAJQhhAIhNhAQgmgfgVgpQgXgrgDgwQgJhhBAhMQBBhMBjgIg");
	this.shape.setTransform(111.9,162.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9BC4E1").s().p("AiZC3QgmgfgVgpQgXgrgDgwQgJhhBAhMQBBhMBjgIQAHgCAMAAQBZAABCA4QAlAfAWAqQAWAqAEAwQAIBjhABLQhABMhjAJIgVABQhUAAhFg5g");
	this.shape_1.setTransform(111.9,162.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#40516C").ss(3).p("ABSAPQgFAhgdAVQgcASgigEQghgHgUgcQgVgcAHghQAGgfAXgTQAXgUAeAAQADAAAKACQAjAGAUAdQAUAcgHAhg");
	this.shape_2.setTransform(29.3,29.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9BC4E1").s().p("AgOBTQghgHgUgcQgVgcAHghQAGgfAXgTQAXgUAeAAIANACQAjAGAUAdQAUAcgHAhQgFAhgdAVQgXAPgaAAIgNgBg");
	this.shape_3.setTransform(29.3,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(624));

	// Layer_9
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(112.2,162.9,1,1,0,0,0,42.9,42.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:360},623).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol13();
	this.instance_1.parent = this;
	this.instance_1.setTransform(112.2,163,1,1,0,0,0,45,45.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:360},623).wait(1));

	// Layer_8
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(29.5,29.4,1,1,0,0,0,26.9,26.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({rotation:720},623).wait(1));

	// Layer_5
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(29.3,29.4,1,1,0,0,0,29.4,29.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:720},623).wait(1));

	// Layer_6
	this.instance_4 = new lib.Symbol8();
	this.instance_4.parent = this;
	this.instance_4.setTransform(47.3,113.2,1.319,1.006,65,0,0,63.3,1.4);

	this.instance_5 = new lib.Symbol8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(92.9,79.2,1.319,1.006,52,0,0,63.3,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5},{t:this.instance_4}]}).wait(624));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-13.7,212,259.2);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#40516C").ss(3).p("AAABYQgjAAgbgaQgagZAAglQAAgkAagZQAbgaAjAAQAkAAAbAaQAaAZAAAkQAAAkgaAaQgbAagkAAg");
	this.shape.setTransform(34.1,34.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9BC4E1").s().p("Ag9A+QgagZAAglQAAgkAagZQAZgaAkAAQAlAAAaAaQAaAZAAAkQAAAkgaAaQgaAaglAAQgkAAgZgag");
	this.shape_1.setTransform(34.1,34.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(236));

	// Layer_6
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(34.1,34.1,1,1,0,0,0,30.6,30.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-360},235).wait(1));

	// Layer_8
	this.instance_1 = new lib.Symbol4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(34,34,1,1,0,0,0,34,34);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:-360},235).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.8,-11.1,128.4,128.4);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol18("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(34.4,18.2,1,1,0,0,0,34.4,20.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22).to({_off:false},0).wait(38));

	// Layer_4
	this.instance_1 = new lib.Symbol18("synched",219);
	this.instance_1.parent = this;
	this.instance_1.setTransform(34.4,18.2,1,1,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(60));

	// Layer_3
	this.instance_2 = new lib.Symbol18("synched",159);
	this.instance_2.parent = this;
	this.instance_2.setTransform(34.4,18.2,1,1,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(60));

	// Layer_2
	this.instance_3 = new lib.Symbol18("synched",99);
	this.instance_3.parent = this;
	this.instance_3.setTransform(34.4,18.2,1,1,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(60));

	// Layer_1
	this.instance_4 = new lib.Symbol18("synched",39);
	this.instance_4.parent = this;
	this.instance_4.setTransform(34.4,18.2,1,1,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(63.7,-1.9,376.3,61.1);


// stage content:
(lib.konveer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E1F5F8").s().p("AikhJQiuhUiegFIAAAAIOcAAQAqAAAbAgQhwAfhXBOQhXBNgrBrQiaiXiyhVg");
	this.shape.setTransform(454.6,52.3,0.667,0.667);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B9E5EF").s().p("AGgCqQhai3icicQididi2hZQiwhXiigFIAAgBIOcAAQAlAAAbAbQAbAaAAAmIAAOeQgFiihXixg");
	this.shape_1.setTransform(455.3,75.4,0.667,0.667);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(402.8,120.3,0.667,0.667,0,0,0,39.3,52.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_3
	this.instance_1 = new lib.Symbol16();
	this.instance_1.parent = this;
	this.instance_1.setTransform(447.2,134,0.667,0.667,0,0,0,23.9,31.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer_5
	this.instance_2 = new lib.Symbol45();
	this.instance_2.parent = this;
	this.instance_2.setTransform(386.3,209.6,1,1,0,0,0,87.3,54.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer_4
	this.instance_3 = new lib.Symbol6();
	this.instance_3.parent = this;
	this.instance_3.setTransform(428.2,116,0.667,0.667,0,0,0,79.2,103.7);

	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(429.9,101.1,0.667,0.667,0,0,0,95.9,9.5);

	this.instance_5 = new lib.Symbol1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(451.5,72.1,0.667,0.667,0,0,0,34.1,34.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5},{t:this.instance_4},{t:this.instance_3}]}).wait(1));

	// Layer_6
	this.instance_6 = new lib.Symbol26();
	this.instance_6.parent = this;
	this.instance_6.setTransform(271.2,106.1,0.667,0.667,0,0,0,132.7,69.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// Layer_9
	this.instance_7 = new lib.Symbol21();
	this.instance_7.parent = this;
	this.instance_7.setTransform(739.8,288.8,0.667,0.667,0,0,0,88,47.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

	// Layer_8
	this.instance_8 = new lib.Symbol20();
	this.instance_8.parent = this;
	this.instance_8.setTransform(452.5,225.8,0.667,0.667,0,0,0,34.4,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

	// Layer_10
	this.instance_9 = new lib.Symbol22();
	this.instance_9.parent = this;
	this.instance_9.setTransform(524.5,165.6,0.667,0.667,0,0,0,35,38.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1));

	// Layer_11
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_2.setTransform(222.2,238.7,0.667,0.667,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_3.setTransform(222,240.3,0.667,0.667,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_4.setTransform(222,240.3,0.667,0.667,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_5.setTransform(242.2,238.7,0.667,0.667,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_6.setTransform(242,240.3,0.667,0.667,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_7.setTransform(242,240.3,0.667,0.667,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_8.setTransform(262.2,238.7,0.667,0.667,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_9.setTransform(262,240.3,0.667,0.667,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_10.setTransform(262,240.3,0.667,0.667,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_11.setTransform(282.2,238.7,0.667,0.667,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_12.setTransform(282.1,240.3,0.667,0.667,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_13.setTransform(282.1,240.3,0.667,0.667,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#94DEA8").s().p("Am7BzQgwAAgqgSQgpgQgdgeIAAgGQAAhBA3gvQA4gvBOAAIP6AAIAADlg");
	this.shape_14.setTransform(251,241.3,0.667,0.667);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#40516C").ss(3).p("AUrifMgsdAAAIAAE/MAsdAAAQBSAAA7gvQA7gvAAhCQAAhBg7gvQg7gvhSAAg");
	this.shape_15.setTransform(309.6,240.2,0.667,0.667,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#6BBD9C").s().p("A3xCgIAAk/MAsbAAAQBTAAA7AvQA7AvgBBBQABBCg7AvQg7AvhTAAg");
	this.shape_16.setTransform(309.6,240.2,0.667,0.667,0,0,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer_14
	this.instance_10 = new lib.Symbol39();
	this.instance_10.parent = this;
	this.instance_10.setTransform(652,64.7,0.667,0.667,0,0,0,37,37);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1));

	// Layer_15
	this.instance_11 = new lib.Symbol42();
	this.instance_11.parent = this;
	this.instance_11.setTransform(433.6,22,1,1,0,0,0,29.4,8.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1));

	// flash0.ai
	this.instance_12 = new lib.Symbol32();
	this.instance_12.parent = this;
	this.instance_12.setTransform(58.3,273.7,0.667,0.667,-90.5,0,0,70.9,43.3);

	this.instance_13 = new lib.Symbol32();
	this.instance_13.parent = this;
	this.instance_13.setTransform(148.3,291.2,0.667,0.667,0,0,0,71,43.3);

	this.instance_14 = new lib.Image();
	this.instance_14.parent = this;
	this.instance_14.setTransform(462.5,282.2,0.127,0.127);

	this.instance_15 = new lib.Design_1();
	this.instance_15.parent = this;
	this.instance_15.setTransform(484.9,360.7,0.667,0.667,0,0,0,511.6,42.6);
	this.instance_15.alpha = 0.148;

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#40516C").s().p("EhQnAAWQgMAAgJgGQgJgHAAgJQAAgJAJgGQAJgHAMAAMChPAAAQAMAAAJAHQAJAGAAAJQAAAJgJAHQgJAGgMAAg");
	this.shape_17.setTransform(411.8,320.8,0.739,0.667);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#40516C").p("AAYgDIgQgTIgXAFIgIAWIAQASIAXgFg");
	this.shape_18.setTransform(543.5,302.4,0.667,0.667);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFF8B9").s().p("AgXAFIAIgWIAXgFIAQATIgIAVIgXAFg");
	this.shape_19.setTransform(543.5,302.4,0.667,0.667);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#40516C").p("AAjAAQAAAPgKAKQgLAKgOAAQgOAAgKgKQgLgKAAgPQAAgOALgKQAKgKAOAAQAOAAALAKQAKAKAAAOg");
	this.shape_20.setTransform(543.5,302.4,0.667,0.667);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CBCED9").s().p("AgYAZQgLgKABgPQgBgNALgLQALgLANAAQAOAAALALQALALgBANQABAPgLAKQgLAKgOAAQgNAAgLgKg");
	this.shape_21.setTransform(543.5,302.4,0.667,0.667);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#40516C").p("AAYgDIgQgTIgXAFIgIAWIAQASIAXgFg");
	this.shape_22.setTransform(543.5,279.7,0.667,0.667);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFF8B9").s().p("AgXAFIAIgWIAXgFIAQATIgIAVIgXAFg");
	this.shape_23.setTransform(543.5,279.7,0.667,0.667);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#40516C").p("AAjAAQAAAPgKAKQgLAKgOAAQgOAAgKgKQgLgKAAgPQAAgOALgKQAKgKAOAAQAOAAALAKQAKAKAAAOg");
	this.shape_24.setTransform(543.5,279.7,0.667,0.667);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CBCED9").s().p("AgYAZQgLgLABgOQgBgNALgLQALgLANAAQAOAAALALQALALgBANQABAOgLALQgLAKgOAAQgNAAgLgKg");
	this.shape_25.setTransform(543.5,279.7,0.667,0.667);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#40516C").p("AAYgDIgQgTIgXAFIgIAWIAQASIAXgFg");
	this.shape_26.setTransform(452.7,302.4,0.667,0.667);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFF8B9").s().p("AgXAFIAIgWIAXgFIAQATIgIAVIgXAFg");
	this.shape_27.setTransform(452.7,302.4,0.667,0.667);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#40516C").p("AAjAAQAAAPgKAKQgLAKgOAAQgOAAgKgKQgLgKAAgPQAAgOALgKQAKgKAOAAQAOAAALAKQAKAKAAAOg");
	this.shape_28.setTransform(452.8,302.4,0.667,0.667);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CBCED9").s().p("AgYAZQgLgKAAgPQAAgNALgLQALgLANAAQAOAAALALQAKALAAANQAAAPgKAKQgLAKgOAAQgNAAgLgKg");
	this.shape_29.setTransform(452.8,302.4,0.667,0.667);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#40516C").p("AAYgDIgQgTIgXAFIgIAWIAQASIAXgFg");
	this.shape_30.setTransform(452.7,279.7,0.667,0.667);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFF8B9").s().p("AgXAFIAIgWIAXgFIAQATIgIAVIgXAFg");
	this.shape_31.setTransform(452.7,279.7,0.667,0.667);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#40516C").p("AAjAAQAAAPgKAKQgLAKgOAAQgOAAgKgKQgLgKAAgPQAAgOALgKQAKgKAOAAQAOAAALAKQAKAKAAAOg");
	this.shape_32.setTransform(452.8,279.7,0.667,0.667);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CBCED9").s().p("AgYAZQgLgLAAgOQAAgNALgLQALgLANAAQAOAAALALQAKALAAANQAAAOgKALQgLAKgOAAQgNAAgLgKg");
	this.shape_33.setTransform(452.8,279.7,0.667,0.667);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#40516C").ss(3).p("ALxDzI3hAAIAAnlIXhAAg");
	this.shape_34.setTransform(498,291.1,0.667,0.667);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("ArwDyIAAnkIXhAAIAAHkg");
	this.shape_35.setTransform(498,291.1,0.667,0.667);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_36.setTransform(663.1,247,0.667,0.667);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_37.setTransform(663.3,248.6,0.667,0.667);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_38.setTransform(663.3,248.6,0.667,0.667);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_39.setTransform(643.1,247,0.667,0.667);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_40.setTransform(643.3,248.6,0.667,0.667);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_41.setTransform(643.3,248.6,0.667,0.667);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_42.setTransform(623.1,247,0.667,0.667);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_43.setTransform(623.3,248.6,0.667,0.667);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_44.setTransform(623.3,248.6,0.667,0.667);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_45.setTransform(603.1,247,0.667,0.667);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_46.setTransform(603.2,248.6,0.667,0.667);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_47.setTransform(603.2,248.6,0.667,0.667);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#F7DE9A").s().p("AhMAYQAAghAXgWQAXgYAhAAQAZAAAVAPQAUAOAJAYQgZgSgeAAQgiAAgaAVQgaAVgHAiQgHgQABgQg");
	this.shape_48.setTransform(583.1,247,0.667,0.667);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#40516C").ss(3).p("AheAAQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_49.setTransform(583.2,248.6,0.667,0.667);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgnAcgbQAcgcAmAAQAnAAAcAcQAcAbAAAnQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_50.setTransform(583.2,248.6,0.667,0.667);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#F7DE9A").s().p("AhMAYQAAggAXgYQAXgXAhAAQAZAAAVAPQAUAOAJAYQgagSgdAAQgiAAgaAVQgaAWgHAhQgHgQABgQg");
	this.shape_51.setTransform(563,246.9,0.667,0.667);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#40516C").ss(3).p("AheAAQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_52.setTransform(563.2,248.5,0.667,0.667);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_53.setTransform(563.2,248.5,0.667,0.667);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#F7DE9A").s().p("AhMAYQAAggAXgYQAXgXAhAAQAZAAAVAPQAUAOAJAYQgagSgdAAQgiAAgaAVQgaAWgHAhQgHgQABgQg");
	this.shape_54.setTransform(543,246.9,0.667,0.667);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#40516C").ss(3).p("AheAAQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_55.setTransform(543.2,248.5,0.667,0.667);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_56.setTransform(543.2,248.5,0.667,0.667);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#F7DE9A").s().p("AhMAYQAAggAXgYQAXgXAhAAQAZAAAVAPQAUAOAJAYQgagSgdAAQgiAAgaAVQgaAWgHAhQgHgQABgQg");
	this.shape_57.setTransform(523,246.9,0.667,0.667);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#40516C").ss(3).p("AheAAQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_58.setTransform(523.2,248.5,0.667,0.667);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_59.setTransform(523.2,248.5,0.667,0.667);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#F7DE9A").s().p("AhMAYQAAggAXgYQAXgXAhAAQAZAAAVAPQAUAOAJAYQgagSgdAAQgiAAgaAVQgaAWgHAhQgHgQABgQg");
	this.shape_60.setTransform(503,246.9,0.667,0.667);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#40516C").ss(3).p("AheAAQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_61.setTransform(503.2,248.5,0.667,0.667);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_62.setTransform(503.2,248.5,0.667,0.667);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#F7DE9A").s().p("AhMAYQAAggAXgYQAXgXAhAAQAZAAAVAPQAUAOAJAYQgagSgdAAQgiAAgaAVQgaAWgHAhQgHgQABgQg");
	this.shape_63.setTransform(483,246.9,0.667,0.667);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#40516C").ss(3).p("AheAAQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcQgcgcAAgng");
	this.shape_64.setTransform(483.1,248.5,0.667,0.667);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFF8B9").s().p("AhCBDQgcgcAAgnQAAgmAcgcQAcgcAmAAQAnAAAcAcQAcAcAAAmQAAAngcAcQgcAcgnAAQgmAAgcgcg");
	this.shape_65.setTransform(483.1,248.5,0.667,0.667);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#94DEA8").s().p("A2aBzIAAjlMAp4AAAQBOAAA3AvQA4AvAABBIgBAFQgcAegoARQgrASgwAAg");
	this.shape_66.setTransform(578.9,249.6,0.667,0.667);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#40516C").ss(3).p("AUrifMgsdAAAIAAE/MAsdAAAQBSAAA7gvQA7gvAAhCQAAhBg7gvQg7gvhSAAg");
	this.shape_67.setTransform(575.8,248.5,0.667,0.667);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#6BBD9C").s().p("A3xCgIAAk/MAsbAAAQBTAAA7AvQA7AvgBBBQABBCg7AvQg7AvhTAAg");
	this.shape_68.setTransform(575.8,248.5,0.667,0.667);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#B4B8C8").s().p("AyWETIAAolMAktAAAIAAIlg");
	this.shape_69.setTransform(396.4,262.9,0.667,0.667);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#DADEEA").s().p("Ag7tBQAzAFAhAnQAjAmAAA0IAAV3QAAA0gjAmQghAngzAFg");
	this.shape_70.setTransform(553.9,255.2,0.667,0.667);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#DADEEA").s().p("AgZMWQgigmAAg0IAA13QAAg0AigmQAignAzgFIAAaDQgzgFgigng");
	this.shape_71.setTransform(313.7,255.2,0.667,0.667);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#CBCED9").s().p("AhlAvQAJgSAAgWQAAgcgSgaIBdAAQAsABAjAaQAkAaANAqg");
	this.shape_72.setTransform(567,316.1,0.667,0.667);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#CBCED9").s().p("AhuAvQANgqAkgZQAkgaArAAIBdAAQgSAYAAAeQAAAUAJATg");
	this.shape_73.setTransform(302.8,316.1,0.667,0.667);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#40516C").ss(3).p("EAg5AA/QgJg2gpgiQgqglg3AAMg9KAAAQg3AAgqAlQgqAigJA2g");
	this.shape_74.setTransform(434.6,316.1,0.667,0.667);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#B4B8C8").s().p("Egg4AA/QAJg2AqgjQAqgkA3AAMA9KAAAQA3AAAqAkQApAjAJA2g");
	this.shape_75.setTransform(434.6,316.1,0.667,0.667);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#40516C").ss(3).p("A9XK8QAAA+AsAsQAsAsA+AAMA2DAAAQA+AAAsgsQAsgsAAg+IAA13QAAg+gsgsQgsgsg+AAMg2DAAAQg+AAgsAsQgsAsAAA+g");
	this.shape_76.setTransform(433.8,255.2,0.667,0.667);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#CBCED9").s().p("A7BNSQg+AAgsgsQgsgsAAg+IAA13QAAg+AsgsQAsgsA+AAMA2DAAAQA+AAAsAsQAsAsAAA+IAAV3QAAA+gsAsQgsAsg+AAg");
	this.shape_77.setTransform(433.8,255.2,0.667,0.667);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#40516C").s().p("AkDAoIAAgeQAAgTAOgPQAQgPAUAAIGjAAQAUAAAPAPQAPAPAAATIAAAeg");
	this.shape_78.setTransform(532.5,196.2,0.667,0.667);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#40516C").s().p("AgnEEIAAoHIAeAAQATAAAQAOQAOAQAAAUIAAGjQAAAUgOAPQgQAPgTAAg");
	this.shape_79.setTransform(496.5,160.2,0.667,0.667);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#40516C").ss(3).p("AFSElQAAkJi7i6Qi7i8kJAAQgSAAgSABIAAE+QAPgBAVAAQCGAABfBdQBdBeAACGQAAAcgEAaIE/AAQACgYAAgeg");
	this.shape_80.setTransform(521,171,0.667,0.667);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#6CBE9D").s().p("AAQFbQAFgaAAgcQAAiFhehfQhehdiGAAQgVAAgPACIAAk/QASgBASAAQEJAAC6C7QC8C7AAEJQAAAegCAYg");
	this.shape_81.setTransform(521,171,0.667,0.667);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#9BC4E1").s().p("AgbAcIAAg3IA3AAIAAA3g");
	this.shape_82.setTransform(451.5,103.3,0.667,0.667);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#40516C").s().p("Au4GXIAAstIdxI4IAAD1g");
	this.shape_83.setTransform(430.3,170.3,0.667,0.667);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#40516C").ss(3).p("AO+T1QAAAmgaAbQgbAagmAAI7GAAQglAAgbgaQgagbAAgmMAAAgnqQAAglAagbQAbgaAlAAIbGAAQAmAAAbAaQAaAbAAAlg");
	this.shape_84.setTransform(429.9,127.6,0.667,0.667);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#9BC4E1").s().p("AtjVQQglAAgbgaQgagbAAgmMAAAgnqQAAglAagbQAbgaAlAAIbGAAQAmAAAbAaQAaAbAAAlMAAAAnqQAAAmgaAbQgbAagmAAg");
	this.shape_85.setTransform(429.9,127.6,0.667,0.667);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(417,200,834,400);
// library properties:
lib.properties = {
	id: 'AC7D1377BDBCD644A55932EB4718E9F2',
	width: 834,
	height: 400,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/images/logo@3x.png", id:"Image"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['AC7D1377BDBCD644A55932EB4718E9F2'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;